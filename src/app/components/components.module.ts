import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { HeaderComponent } from './header/header.component';
import { CarritoPage } from '../pages/carrito/carrito.page';
import { PopoverInfoComponent } from './popover-info/popover-info.component';
import { ServiciosComponent } from './regresos/servicios/servicios.component';
import { BuscarComponent } from './buscar/buscar.component';

@NgModule({
  declarations: [
    HeaderComponent,
    CarritoPage,
    PopoverInfoComponent
  ],
  exports: [
    HeaderComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule,
  ]
})
export class ComponentsModule { }
