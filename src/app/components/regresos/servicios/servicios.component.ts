import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-servicios',
  templateUrl: './servicios.component.html',
  styleUrls: ['./servicios.component.scss'],
})
export class ServiciosComponent implements OnInit {

  @Input() servicio= '';
  
  constructor() { }

  ngOnInit() {}

}
