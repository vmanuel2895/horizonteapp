import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tarjeta',
  templateUrl: './tarjeta.component.html',
  styleUrls: ['./tarjeta.component.scss'],
})
export class TarjetaComponent implements OnInit {

    titular:string;
    tarjeta:string = '0000 0000 0000 0000';
    mes: any;
    ano:any;
    cvv:string;

  constructor() { }

  ngOnInit() {}

  numeroTarjeta(value) {
    this.tarjeta = value;
  }

  nombreTitular(value) {
    this.titular = value;
  }

  anos(event){
    let eve = event.detail.value
    let fecha = new Date(eve)
    this.ano = fecha.getFullYear().toString().slice(2, 4)       
  }

  meses(event){
    let eve =  event.detail.value.split('T')[0]
    let fecha = new Date (eve);
    this.mes= fecha.getMonth()+1;
    
    /* this.mes=fecha.getMonth()
    console.log(this.mes); */
           
  }
}
