import { Component, OnInit } from '@angular/core';
import {NavParams} from '@ionic/angular';
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-ambulancia',
  templateUrl: './ambulancia.component.html',
  styleUrls: ['./ambulancia.component.scss'],
})
export class AmbulanciaComponent implements OnInit {

  public datos = {
    nombre:"",
    dia:"",
    noche:"",
    membresiadia:"",
    membresianoche:""
  }
  constructor(public navParams:NavParams, public _controler:PopoverController, private _storage: Storage, private _route:Router) {}

  ngOnInit() {
    this.datos.nombre=this.navParams.data.DESTINO
    this.datos.dia=this.navParams.data.PRECIO_PUBLICO_DIA
    this.datos.noche=this.navParams.data.PRECIO_PUBLICO_NOCHE
    this.datos.membresiadia=this.navParams.data.PRECIO_MEMBRESIA_DIA
    this.datos.membresianoche=this.navParams.data.PRECIO_MEMBRESIA_NOCHE
  }

  async DismissClick() {
    await this._controler.dismiss();
  }

  onClick(){
    this.DismissClick()
    localStorage.setItem('membresia', "true");
    
    this._route.navigate(['/sign-in'])
  }
}
