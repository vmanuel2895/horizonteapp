import { Component, OnInit } from '@angular/core';
import {NavParams} from '@ionic/angular';
import { PopoverController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-popover-info',
  templateUrl: './popover-info.component.html',
  styleUrls: ['./popover-info.component.scss'],
})
export class PopoverInfoComponent implements OnInit {
  
  public datos = {
    nombre:"",
    precio_membresia:"",
    precio_publico:""
  }

  public membresia:boolean;

  constructor(public navParams:NavParams, public _controler:PopoverController, private _storage: Storage, private _route:Router) {}

  ngOnInit() {
    console.log(this.navParams);
    
    this.datos.nombre=this.navParams.data.item.ESTUDIO
    this.datos.precio_membresia=this.navParams.data.item.PRECIO_MEMBRESIA
    this.datos.precio_publico=this.navParams.data.item.PRECIO_PUBLICO
    this.membresia=this.navParams.data.membresia;
  }

  async DismissClick() {
    await this._controler.dismiss();
  }

  onClick(){
    this.DismissClick()
    localStorage.setItem('membresia', "true");
    
    this._route.navigate(['/sign-in'])
  }

}
