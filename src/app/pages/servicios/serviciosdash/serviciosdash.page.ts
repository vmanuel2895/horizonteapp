import { Component, OnInit } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
  selector: 'app-serviciosdash',
  templateUrl: './serviciosdash.page.html',
  styleUrls: ['./serviciosdash.page.scss'],
})
export class ServiciosdashPage implements OnInit {


  constructor(private _callNumber: CallNumber) { }

  ngOnInit() {
  }


  onClick(){
    this._callNumber.callNumber("7353577564", true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }

  openWhatsapp(phone:string):void {
      window.open('https://api.whatsapp.com/send?phone='+phone+'&text=Hola,%20horizonte', '_system');
  }

}
