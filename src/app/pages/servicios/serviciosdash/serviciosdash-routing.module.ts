import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ServiciosdashPage } from './serviciosdash.page';

const routes: Routes = [
  {
    path:'',
    redirectTo: '/serviciosdash/proceedings',
    pathMatch: 'full',
  },
  {
    path: '',
    component: ServiciosdashPage,
    children: [
      {
        path: 'services',
        loadChildren: () => import('../tabs/servicios/servicios.module').then( m => m.ServiciosPageModule )
      },
      {
        path: 'proceedings',
        loadChildren: () => import('../tabs/proceedings/proceedings.module').then( m => m.ProceedingsPageModule )
      },
      {
        path: 'settings',
        loadChildren: () => import('../tabs/ajustes/ajustes.module').then( m => m.AjustesPageModule )
      },
      {
        path: 'premium',
        loadChildren: () => import('../tabs/premium/premium.module').then( m => m.PremiumPageModule )
      },
      {
        path: 'chat',
        loadChildren: () => import('../tabs/chat/chat.module').then( m => m.ChatPageModule )
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ServiciosdashPageRoutingModule {}
