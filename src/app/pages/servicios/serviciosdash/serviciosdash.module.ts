import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ServiciosdashPageRoutingModule } from './serviciosdash-routing.module';

import { ServiciosdashPage } from './serviciosdash.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ServiciosdashPageRoutingModule
  ],
  declarations: [ServiciosdashPage]
})
export class ServiciosdashPageModule {}
