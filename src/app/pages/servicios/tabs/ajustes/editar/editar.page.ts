import { Component, OnInit } from '@angular/core';
import { PacienteService } from '../../../../../services/paciente/paciente.service';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.page.html',
  styleUrls: ['./editar.page.scss'],
})
export class EditarPage implements OnInit {

  constructor(public paciente:PacienteService) { }

  ngOnInit() {
  }

}
