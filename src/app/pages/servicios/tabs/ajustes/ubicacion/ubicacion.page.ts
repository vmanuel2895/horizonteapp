import { Component, OnInit } from '@angular/core';

declare var google;

@Component({
  selector: 'app-ubicacion',
  templateUrl: './ubicacion.page.html',
  styleUrls: ['./ubicacion.page.scss'],
})
export class UbicacionPage implements OnInit {

  map = null;
  markers: Marker[] = [
    {
      position: {
        lat: 18.823354332996892,
        lng: -98.94876990248297,
      },
      title: 'HORIZONTE GRUPO DE SALUD MEDICA SA DE CV'
    },
    {
      position: {
        lat: 18.95514789690965,
        lng: -98.9841278167583,
      },
      title: 'CENTRO MÉDICO HORIZONTE'
    }
  ];

  constructor() { }

  ngOnInit() {
    this.loadMap()
  }

  loadMap() { 
    // create a new map by passing HTMLElement
    const mapEle: HTMLElement = document.getElementById('map');
    // create LatLng object
    const myLatLng = {lat: 18.95514789690965, lng: -98.9841278167583};
    // create map
    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 10
    });

    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      this.renderMarkers();
      mapEle.classList.add('show-map');
    });    
  }

  renderMarkers() {
    this.markers.forEach(marker => {
      this.addMarker(marker);
    });
  }

  addMarker(marker: Marker) {
    return new google.maps.Marker({
      position: marker.position,
      map: this.map,
      title: marker.title
    });
  }

}

interface Marker {
  position: {
    lat: number,
    lng: number,
  };
  title: string;
}
