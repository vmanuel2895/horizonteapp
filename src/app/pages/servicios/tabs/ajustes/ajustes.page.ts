import { Component, OnInit, Renderer2 } from '@angular/core';
import { LoginServices } from '../../../../services/logeo/login.service';
import { PacienteService } from '../../../../services/paciente/paciente.service';
import { Clipboard } from '@ionic-native/clipboard/ngx';

@Component({
  selector: 'app-ajustes',
  templateUrl: './ajustes.page.html',
  styleUrls: ['./ajustes.page.scss'],
})
export class AjustesPage implements OnInit {

  public tema = '';
  /* usuario={}; */

  constructor(public paciente: PacienteService, 
              private _login:LoginServices, 
              public render:Renderer2,
              private clipboard: Clipboard) {}

  ngOnInit(){
    /* this.usuario= JSON.parse(obtenerUsuario()) */
  }

  copy(texto){
    console.log(texto);
    
    this.clipboard.copy(texto);
  }

  ThemePink(){
      document.body.classList.toggle('pink');
      //this.render.setAttribute(document.body, 'color-theme','pink');
    /* }
    else{
      this.render.setAttribute(document.body, 'color-theme','light');
    } */
  }

  onToggleColorThemes(event){
    console.log(event);
    
    if(event.detail == 1){
      this.render.setAttribute(document.body, 'color-theme','pinks');
    }else{
      this.render.setAttribute(document.body, 'color-theme','light');
    }
  }

  Theme(bodyClass){
    //console.log(event);
    if(this.tema != ''){ 
      this.render.removeClass(document.body, this.tema);
    }
    switch(bodyClass){
      case 'pink':
        document.body.classList.toggle('pink');
        document.getElementById("principal").style.background= "#b771b5";
        this.tema=bodyClass;
        break;
      case 'dark':
        //this.render.addClass(document.body, bodyClass);
        document.body.classList.toggle('dark');
        document.getElementById("principal").style.background= "#222428";
        this.tema=bodyClass;
        break;
      case 'azul':
        this.render.removeClass(document.body, this.tema);
        document.getElementById("principal").style.background= "#273C74";
        break;
      case 'red':
        document.body.classList.toggle('red');
        document.getElementById("principal").style.background= "#ee6868";
        this.tema=bodyClass;
        break;
      case 'green':
        document.body.classList.toggle('green');
        document.getElementById("principal").style.background= "#4b8b56";
        this.tema=bodyClass;
        break;
    }    
    /* this.render.removeClass(document.body, bodyClass);
    document.body.classList.toggle('dark'); */
  }

  onClick(){
    if(this.tema != ''){ 
      this.render.removeClass(document.body, this.tema);
    }
      this._login.logout();
  }

  slidesOptions = {
    slidesPerView:5
  }


}
