import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PacienteService } from '../../../../../services/paciente/paciente.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nuevo',
  templateUrl: './nuevo.page.html',
  styleUrls: ['./nuevo.page.scss'],
})
export class NuevoPage implements OnInit {
  
  public familia = { 
            nombre: '',
            integrantes: ''
          }
  public id={
    id:''
  };
  public idFamilia;
  //public familias:[];
  constructor( public paciente: PacienteService, public _route:Router ) { 
    console.log(paciente);
    
  }

  ngOnInit() {
    this.familias();
  }

  familias(){
    this.paciente.cargarFamilias().subscribe((resp:any)=>{
      if (resp.ok) {
        if (resp.data.length == 0) {
          this.idFamilia='';
        }else{
          this.setidFamilia(resp.data[0]._id);
        }        
      }
    })
  }

  setidFamilia(id){
    this.idFamilia=id
  }

  async onSubmit( formulario: NgForm ) {
    if(this.idFamilia != ''){ 
      this.paciente.updateFamilias(this.idFamilia,formulario.form.value).subscribe((data)=>{
        console.log(data);
        //console.log(localStorage.getItem('idFamilia'));
        this.paciente.getFamilia(this.idFamilia).subscribe(resp=>{
          if(resp.ok){
            localStorage.setItem('Familia', JSON.stringify(resp.data))
            this.paciente.cargarFamilia();
            this._route.navigate(['/serviciosdash/proceedings']);
            }
          }) 
      });      

    }else{

      this.familia.nombre= this.paciente.usuario._id
      this.familia.integrantes= formulario.form.value.integrantes
      
      this.paciente.familias(this.familia).subscribe(res => {
        if(res.ok){
          localStorage.setItem('idFamilia', res.data._id);
          this.paciente.cargarIdFamilia(); 
        }
          this.paciente.getFamilia(localStorage.getItem('idFamilia')).subscribe(resp=>{
            if(resp.ok){
              localStorage.setItem('Familia', JSON.stringify(resp.data))
              this.paciente.cargarFamilia();
              this._route.navigate(['/serviciosdash/proceedings']);
              localStorage.removeItem('idFamilia')
            }
          }) 
      }); 
    }
    
    
  }

}
