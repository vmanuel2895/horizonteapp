import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SignosPageRoutingModule } from './signos-routing.module';

import { SignosPage } from './signos.page';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChartsModule,
    SignosPageRoutingModule
  ],
  declarations: [SignosPage]
})
export class SignosPageModule {}
