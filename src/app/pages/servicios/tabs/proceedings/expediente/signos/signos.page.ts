import { Component, OnInit} from '@angular/core';
import { PacienteService } from '../../../../../../services/paciente/paciente.service';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import {TomaSignosPage} from './toma-signos/toma-signos.page'
@Component({
  selector: 'app-signos',
  templateUrl: './signos.page.html',
  styleUrls: ['./signos.page.scss'],
})
export class SignosPage implements OnInit {

  public idPaciente;
  public signos=[
    {
      peso: '',
      talla: '',
      pa: '',
      fc: '',
      temp: '',
      SaO: '',
      pao: '',
      imc:'',
      glucosa: '',
      tomadesignos: ''
    }
  ];
 
  constructor(public _paciente:PacienteService, private _router: Router, private modalCtrl:ModalController) {}

  ngOnInit() {
    this.obtenerIdPaciente();
    this.obtenerSignosVitales();
  }  

  obtenerIdPaciente(){
    this.idPaciente = localStorage.getItem('idPaciente')
  }
  
  obtenerSignosVitales(){
    this._paciente.obtenerHistroialSignosVitalesPaciente(this.idPaciente).subscribe((resp:any)=>{
      if(resp.ok){
        this.setSignos(resp['data'])
      }
    })
  }

  setSignos(resp){
    console.log(resp);
     
    if(resp.length == 0){
      this.signos=[
        {
          peso: '0',
          talla: '0',
          pa: '0',
          fc: '0',
          temp: '0',
          SaO: '0',
          pao: '0',
          imc:'0',
          glucosa: '0',
          tomadesignos: 'APP'
        }
      ];
    }else{
      this.signos= resp;
      this.signos.reverse();
      localStorage.setItem('signos', JSON.stringify(this.signos[0]))
    }
  }

  async mostrarModal() {

    const modal = await this.modalCtrl.create({
      component: TomaSignosPage,
      /* componentProps: {
        item,
        membresia
      } */
    });
    
    await modal.present();
    const { data } = await modal.onWillDismiss();
    this.signos = data.datos
    localStorage.setItem('signos', JSON.stringify(this.signos))
  }
}
