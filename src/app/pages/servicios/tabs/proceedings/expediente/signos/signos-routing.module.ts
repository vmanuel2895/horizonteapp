import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SignosPage } from './signos.page';

const routes: Routes = [
  {
    path: '',
    component: SignosPage
  },
  {
    path: 'graficas/:nombre',
    loadChildren: () => import('./graficas/graficas.module').then( m => m.GraficasPageModule)
  },
  {
    path: 'toma-signos',
    loadChildren: () => import('./toma-signos/toma-signos.module').then( m => m.TomaSignosPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SignosPageRoutingModule {}
