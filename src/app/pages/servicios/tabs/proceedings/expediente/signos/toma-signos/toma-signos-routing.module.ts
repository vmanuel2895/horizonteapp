import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TomaSignosPage } from './toma-signos.page';

const routes: Routes = [
  {
    path: '',
    component: TomaSignosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TomaSignosPageRoutingModule {}
