import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SignosPage } from './signos.page';

describe('SignosPage', () => {
  let component: SignosPage;
  let fixture: ComponentFixture<SignosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SignosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
