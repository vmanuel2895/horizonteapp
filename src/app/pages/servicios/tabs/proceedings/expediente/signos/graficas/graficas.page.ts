import { Component, OnInit, ViewChild} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import { PacienteService } from '../../../../../../../services/paciente/paciente.service';
import * as zoomPlugin from 'chartjs-plugin-zoom';

@Component({
  selector: 'app-graficas',
  templateUrl: './graficas.page.html',
  styleUrls: ['./graficas.page.scss'],
})
export class GraficasPage implements OnInit {

  public grafica;
  public idPaciente;
  public peso;
  public talla;
  public imc;
  public temp;
  public sistolica;
  public diastolica;
  public sao;
  public glucosa;
  public edad;
  public genero;
  constructor(private _route: ActivatedRoute, public _paciente: PacienteService) { }

  ngOnInit() {
    this.grafica = this._route.snapshot.paramMap.get('nombre');
    this.obtenerIdPaciente();
    this.eleccionGrafica(this.grafica); 
  }

  eleccionGrafica(grafica){
    if(grafica == 'peso'){
      this.graficaPeso();
    }else if(grafica == 'talla'){
      this.graficaTalla();
    }else if(grafica == 'imc'){
      this.graficaIMC();
    }else if(grafica == 'temp'){
      this.graficaTemp();
    }else if(grafica == 'pa'){
      this.graficarPA();
    }else if(grafica == 'sao'){
      this.graficarSAO();
    }else if(grafica == 'glucosa'){
      this.graficarGlucosa();
    }
  }

  obtenerIdPaciente(){
    this.idPaciente = localStorage.getItem('idPaciente')
    this.obtenerPaciente(this.idPaciente);
  }

  obtenerPaciente(id){
    this._paciente.getPacienteBtID(id)
    .subscribe( (data:any) => {      
      this.edad=data.paciente.edad;
      this.genero = data.paciente.genero;
      
      this.recorrerEdadNinoPeso(this.edad);
      this.recorrerEdadNinoTalla(this.edad);
      this.recorrerEdadNinoIMC(this.edad);
      this.recorrerEdadNinaPeso(this.edad);
      this.recorrerEdadNinaTalla(this.edad);
      this.recorrerEdadNinaIMC(this.edad);
    });
  }

  obtenerUltimoPeso(data){
    data.reverse();
    this.peso = data[0].peso;
  }

  obtenerUltimaTalla(data){
    data.reverse();
    this.talla = data[0].talla;
  }

  obtenerUltimoIMC(data){
    data.reverse();
    this.imc = data[0].imc;
  }
  obtenerUltimaTemp(data){
    data.reverse();
    this.temp = data[0].temp;
  }

  obtenerUltimaPresion(data){
    data.reverse();
    this.sistolica = data[0].sistolica;
    this.diastolica = data[0].sistolica;
  }

  obtenerUltimaSAO(data){
    data.reverse();
    this.sao = data[0].SaO;
  }

  obtenerUltimaGlucosa(data){
    data.reverse();
    this.glucosa = data[0].glucosa;
  }

  graficaPeso(){
    this._paciente.obtenerHistroialSignosVitalesPaciente(this.idPaciente)
      .subscribe((data) => {
        let grafPeso = data['data'];
        console.log(grafPeso);
        grafPeso.forEach(element => {
          if (element == null) {
            return;
          }else{
            if(element.tomadesignos == 'APP'){
              if(element.peso == undefined){
                this.linePeso[1].data.push(0)
                this.linePeso[0].data.push(0)
              }else{
                this.linePeso[1].data.push(element.peso)
                this.linePeso[0].data.push(0)
              } 
            }else{
              if(element.peso == undefined){
                this.linePeso[1].data.push(0)
                this.linePeso[0].data.push(0)
              }else{
                this.linePeso[1].data.push(0)
                this.linePeso[0].data.push(element.peso)
              }
            }        
            this.lineChartLabels.push(element.fechaIngreso)
          }
          console.log(this.lineChartLabels);
          
        });
        this.obtenerUltimoPeso(data['data']);
      });
  }

  graficaTalla() {
    this._paciente.obtenerHistroialSignosVitalesPaciente(this.idPaciente)
      .subscribe((data) => {
        let grafTalla = data['data'];
        console.log(data);
        
        grafTalla.forEach(element => {
          if (element == null) {
            return;
          } else {
            if(element.tomadesignos == 'APP'){
              if(element.talla == undefined){
                this.lineChartData[0].data.push(0)
                this.lineChartData[1].data.push(0)
              }else{
                this.lineChartData[0].data.push(0)
                this.lineChartData[1].data.push(element.talla)
              } 
            }else{
              if(element.talla == undefined){
                this.lineChartData[1].data.push(0)
                this.lineChartData[0].data.push(0)
              }else{
                this.lineChartData[1].data.push(0)
                this.lineChartData[0].data.push(element.talla)
              }
            }
            this.lineChartLabels.push(element.fechaIngreso)
          }
        });
        this.obtenerUltimaTalla(data['data']);
      });
  }

  graficaIMC() {
    this._paciente.obtenerHistroialSignosVitalesPaciente(this.idPaciente)
      .subscribe((data) => {
        // console.log(data);

        let grafIMC = data['data'];

        grafIMC.forEach(element => {
          if (element == null) {
            return;
          } else {
            if(element.tomadesignos == 'APP'){
              if(element.imc == undefined){
                this.lineIMC[1].data.push(0)
                this.lineIMC[0].data.push(0)
              }else{
                this.lineIMC[1].data.push(element.imc)
                this.lineIMC[0].data.push(0)
              }
            }else{
              if(element.imc == undefined){
                this.lineIMC[0].data.push(0)
                this.lineIMC[1].data.push(0)
              }else{
                this.lineIMC[1].data.push(0)
                this.lineIMC[0].data.push(element.imc)
              }
            }
            this.lineChartLabels.push(element.fechaIngreso)
          }
        });
        this.obtenerUltimoIMC(data['data']);
      });
  }

  graficaTemp() {
    this._paciente.obtenerHistroialSignosVitalesPaciente(this.idPaciente)
      .subscribe((data) => {
        let graf = data['data'];
        graf.forEach(element => {
          if (element == null) {
            return;
          } else {
            if(element.tomadesignos == 'APP'){
              if(element.temp == undefined){
                this.lineTemp[0].data.push(0);
                this.lineTemp[1].data.push(0);
              }else{
                this.lineTemp[0].data.push(0);
                this.lineTemp[1].data.push(element.temp);
              }
            }else{
              if(element.temp == undefined){
                this.lineTemp[0].data.push(0);
                this.lineTemp[1].data.push(0);
              }else{
                this.lineTemp[0].data.push(element.temp);
                this.lineTemp[1].data.push(0);
              }
            }
            this.lineChartLabels.push(element.fechaIngreso)
          }
        });
        this.obtenerUltimaTemp(data['data']);
      });
  }

  graficarPA() {
    this._paciente.obtenerHistroialSignosVitalesPaciente(this.idPaciente)
      .subscribe((data) => {
        let graPA = data['data'];
        graPA.forEach(element => {
          if (element == null) {
            return;
          } else {
            if(element.tomadesignos == 'APP'){
              if(element.sistolica == undefined && element.diastolica == undefined){
                this.linePA[0].data.push(0)
                this.linePA[1].data.push(0)
                this.linePA[2].data.push(0)
                this.linePA[3].data.push(0)
                this.linePA[4].data.push(140)
                this.linePA[5].data.push(90)
              }else{
                this.linePA[0].data.push(0)
                this.linePA[1].data.push(0)
                this.linePA[2].data.push(element.sistolica)
                this.linePA[3].data.push(element.diastolica)
                this.linePA[4].data.push(140)
                this.linePA[5].data.push(90)
              }
            }else{
              if(element.pa == undefined){
                this.linePA[0].data.push(0)
                this.linePA[1].data.push(0)
                this.linePA[2].data.push(0)
                this.linePA[3].data.push(0)
                this.linePA[4].data.push(140)
                this.linePA[5].data.push(90)
              }else{
                this.linePA[0].data.push(element.sistolica)
                this.linePA[1].data.push(element.diastolica)
                this.linePA[2].data.push(0)
                this.linePA[3].data.push(0)
                this.linePA[4].data.push(140)
                this.linePA[5].data.push(90)
              }
            } 
            this.lineChartLabels.push(element.fechaIngreso)
          }
        });
        this.obtenerUltimaPresion(data['data']);
      });
  }

  graficarSAO() {
    this._paciente.obtenerHistroialSignosVitalesPaciente(this.idPaciente)
      .subscribe((data) => {
        let graSAO = data['data'];
        graSAO.forEach(element => {
          if (element == null) {
            return;
          } else {
            if(element.tomadesignos == 'APP'){
              if(element.pao == undefined){
                this.lineSAO[1].data.push(0)
                this.lineSAO[0].data.push(0)
              }else{
                this.lineSAO[1].data.push(element.pao)
                this.lineSAO[0].data.push(0)
              }
            }else{
              if(element.pao == undefined){
                this.lineSAO[0].data.push(0)
                this.lineSAO[1].data.push(0)
              }else{
                this.lineSAO[0].data.push(element.pao)
                this.lineSAO[1].data.push(0)
              }
            }
            this.lineChartLabels.push(element.fechaIngreso)
          }
        });
        this.obtenerUltimaSAO(data['data']);
      });
  }

  graficarGlucosa() {
    this._paciente.obtenerHistroialSignosVitalesPaciente(this.idPaciente)
      .subscribe((data) => {
        let graGluc = data['data'];
        console.log(graGluc);
        
        graGluc.forEach(element => {
          if (element == null) {
            return;
          } else {
            if(element.tomadesignos == 'APP'){
              if(element.glucosa == undefined){
                this.lineGlucosa[1].data.push(0)
                this.lineGlucosa[0].data.push(0)
              }else{
                this.lineGlucosa[1].data.push(element.glucosa)
                this.lineGlucosa[0].data.push(0)
              }
            }else{
              if(element.glucosa == undefined){
                this.lineGlucosa[0].data.push(0)
                this.lineGlucosa[1].data.push(0)
              }else{
                this.lineGlucosa[0].data.push(element.glucosa)
                this.lineGlucosa[1].data.push(0)
              }
            }
            this.lineChartLabels.push(element.fechaIngreso)
          }
        });
        this.obtenerUltimaGlucosa(data['data']);
      });
  }

  //graficas niños
  recorrerEdadNinoPeso(edad){
    let sig = JSON.parse( localStorage.getItem('signos') );
    let edadEscala = edad;
    for(let i=0; i < edadEscala; i++){
      this.lineChartDataPeso[2].data.push(0);
      if(this.lineChartDataPeso[2].data.length == edadEscala){
        this.lineChartDataPeso[2].data.push(sig.peso);
      }
    }
  }

  recorrerEdadNinoTalla(edad){
    let sig = JSON.parse( localStorage.getItem('signos') );
    let edadEscala = edad;
    for(let i=1; i <= edadEscala; i++){
      this.lineChartEstatura[2].data.push(0)
      if(i == edadEscala){
        this.lineChartEstatura[2].data.push(sig.talla);
      }
    }
  }

  recorrerEdadNinoIMC(edad){
    let sig = JSON.parse( localStorage.getItem('signos') );
    let edadEscala = edad;
    for(let i=0; i < edadEscala; i++){
      this.lineChartIMC[2].data.push(0);
      if(this.lineChartIMC[2].data.length == edadEscala){
        this.lineChartIMC[2].data.push(sig.imc);
      }
    }
  }
  //fin grafica niños

  //graficas niñas
  recorrerEdadNinaPeso(edad){
    let edadEscala = edad;
    let sig = JSON.parse( localStorage.getItem('signos') );
    for(let i=0; i < edadEscala; i++){
      this.lineChartPesoNina[2].data.push(0);


      if(this.lineChartPesoNina[2].data.length == edadEscala){
        this.lineChartPesoNina[2].data.push(sig.peso);
      }
    }
  }

  recorrerEdadNinaTalla(edad){
    let edadEscala = edad;
    let sig = JSON.parse( localStorage.getItem('signos') );
    for(let i=0; i < edadEscala; i++){
      this.lineChartTallaNina[2].data.push(0);


      if(this.lineChartTallaNina[2].data.length == edadEscala){
        this.lineChartTallaNina[2].data.push(sig.talla);
      }
    }
  }

  recorrerEdadNinaIMC(edad){
    let edadEscala = edad;
    let sig = JSON.parse( localStorage.getItem('signos') );
    for(let i=0; i < edadEscala; i++){
      this.lineChartIMCNina[2].data.push(0);

      if(this.lineChartIMCNina[2].data.length == edadEscala){
        this.lineChartIMCNina[2].data.push(sig.imc);
      }
    }
  }
  //fin graficas niñas
  // Graficas
  public linePeso: ChartDataSets[] = [
    { data: [], label: 'Horizonte'},
    { data: [], label: 'Paciente'}
  ];

  public lineChartData: ChartDataSets[] = [
    { data: [], label: 'Horizonte' },
    { data: [], label: 'Paciente'}
  ];

  public lineIMC: ChartDataSets[] = [
    { data: [], label: 'Horizonte' },
    { data: [], label: 'Paciente'}
  ];

  public lineTemp: ChartDataSets[] = [
    { data: [], label: 'Horizonte' },
    { data: [], label: 'Paciente'}
  ];

  public linePA: ChartDataSets[] = [
    { data: [], label: 'Horizonte Sistolica' },
    { data: [], label: 'Horizonte Diastólica' },
    { data: [], label: 'APP Sistólica' },
    { data: [], label: 'APP Diastólica' },
    { data: [], label: 'Sistólica Máxima' },
    { data: [], label: 'Diastólica Máxima' }
  ];

  public lineChartOptionssistolica: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        },
        {
          id: 'y-axis-1',
          position: 'right',
          gridLines: {
            color: 'rgba(255,0,0,0.3)',
          },
          ticks: {
            fontColor: 'red',
          }
        }
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  };

  public lineChartColorssistolica: Color[] = [
    { // blue
      // backgroundColor: 'rgba(50, 87, 231, 0.9)',
      borderColor: 'blue',
      pointBackgroundColor: 'blue',
      pointBorderColor: 'blue',
      // pointHoverBackgroundColor: '#fff',
      // pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    {
      // backgroundColor: 'rgba(53, 126, 87, 0.9)',
      borderColor: 'green',
      pointBackgroundColor: 'green',
      pointBorderColor: 'green',
    },
    {
      borderColor: 'red',
      pointBackgroundColor: 'red',
      pointBorderColor: 'red',
    },
    {
      borderColor: 'yellow',
      pointBackgroundColor: 'yellow',
      pointBorderColor: 'yellow'
    },
    {
      borderColor: 'black',
      pointBackgroundColor: 'black',
      pointBorderColor: 'black'
    },
    {
      borderColor: 'purple',
      pointBackgroundColor: 'purple',
      pointBorderColor: 'purple'
    }
  ];

  public lineSAO: ChartDataSets[] = [
    { data: [], label: 'Horizonte'},
    { data: [], label: 'Paciente'}
  ];

  public lineGlucosa: ChartDataSets[] = [
    { data: [], label: 'Historial Glucosa' },
    { data: [], label: 'Paciente'}
  ];

  //grafica niños peso
    //peso
      public lineChartDataPeso: ChartDataSets[] = [
        { data: [10.673, 11.633, 13.291, 14.990, 16.500, 17.900, 19.900, 22.076, 24.380, 26.562, 29.472, 32.600, 36.712, 41.149, 45.459, 49.866, 51.69, 53.148, 54.200], label: 'Peso Mínimo' },
        { data: [15.668, 18.045, 20.739, 24.461, 28.271, 32.149, 36.983, 39.666, 46.566, 48.863, 56.260, 63.308, 69.705, 76.420, 82.735, 88.504, 93.779, 97.245, 99.185, 102.300], label: 'Peso Máximo' },
        { data:[], label: 'Peso Actual'},
      ];
      public lineChartLabels2: Label[] = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18'];
      public lineChartColors2: Color[] = [
        { // Rosa Bajito
          // backgroundColor: 'rgba(148,159,177,0.2)',
          borderColor: ' #0f5be7',
          // pointBackgroundColor: '#ea3af0',
          // pointBorderColor: '#ea3af0',
          // pointHoverBackgroundColor: '#fff',
          // pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        },
        { // dark grey
          // backgroundColor: 'rgba(0,0,255,1)',
          borderColor: ' #3a72db',
          // pointBackgroundColor: ' #f40bfc',
          // pointBorderColor: ' #f40bfc',
          // pointHoverBackgroundColor: '#fff',
          // pointHoverBorderColor: 'rgba(77,83,96,1)'
        },
        { // red
          // backgroundColor: 'rgba(0,255,0,0.6)',
          borderColor: ' #0e3275',
          // pointBackgroundColor: 'rgb(241, 29, 171)',
          // pointBorderColor: 'green',
          // pointHoverBackgroundColor: 'green',
          // pointHoverBorderColor: 'rgba(0,255,0,0.6)'
        }
      ];
      public lineChartOptionsninos: (ChartOptions & { annotation: any }) = {
        responsive: true,
        scales: {
          // We use this empty structure as a placeholder for dynamic theming.
          xAxes: [{}],
          yAxes: [
            {
              id: 'y-axis-0',
              position: 'left',
            },
            {
              id: 'y-axis-1',
              position: 'right',
              gridLines: {
                color: 'rgba(0,0,2555,0.2)',
              },
              ticks: {
                fontColor: 'black',
              }
            }
          ]
        },
        annotation: {
          annotations: [
            {
              type: 'line',
              mode: 'vertical',
              scaleID: 'x-axis-0',
              value: 'March',
              borderColor: 'orange',
              borderWidth: 2,
              label: {
                enabled: true,
                fontColor: 'orange',
                content: 'LineAnno'
              }
            },
          ],
        },
      };
    //fin peso

    //talla
      public lineChartEstatura: ChartDataSets [] = [
        { data: [.80, .84, .88, .96, 1.02 , 1.08, 1.15, 1.20, 1.26, 1.30, 1.33, 1.36, 1.43, 1.45, 1.50, 1.57, 1.58, 1.60, 1.62, 1.63, 1.66], label: 'Estatura Mínima'},
        { data: [.93, 1.03, 1.12, 1.20, 1.28, 1.33, 1.36, 1.44, 1.50, 1.54, 1.57, 1.64, 1.70, 1.73, 1.76, 1.80, 1.83, 1.85, 1.88, 1.89, 1.90], label: 'Estatura Máxima'},
        { data: [ ], label: 'Estatura Actual'}
      ];
    //fin talla

    //imc
      public lineChartIMC: ChartDataSets [] = [
        { data: [14.5, 14.09, 13.81, 13.64, 13.54, 13.52, 13.57, 13.78, 13.96, 14.28, 14.68, 15.10, 15.66, 16.16, 16.77, 17.28, 17.85, 18.30, 19.21], label: 'IMC Mínimo'},
        { data: [19.66, 18.69, 18.23, 18.44, 19.10, 20.08, 21.23, 22.37, 23.72, 24.92, 26.03, 27.02, 27.88, 28.63, 29.30, 29.88, 30.61, 31.20, 32.10], label: "IMC Máximo"},
        { data: [ ], label: "IMC Actual"}
      ];
    //fin imc
  //fin grafica niños peso
  // grafica niña peso
  public lineChartPesoNina: ChartDataSets [] = [
    { data: [10.17, 11.38, 12.65, 14.34, 16.01, 17.72, 19.64, 21.58, 23.99, 26.81, 29.74, 33.12, 36.70, 39.37, 41.82, 43.23, 44.24, 45.01, 45.12], label: 'Peso Mínimo'},
    { data: [15.35, 18.0, 21.28, 24.62, 28.92, 33.36, 38.07, 44.58, 51.42, 58.10, 65.32, 71.87, 77.68, 81.36, 84.36, 86.04, 87.34, 88.15, 88.20], label: 'Peso Máximo'},
    { data: [ ], label: 'Peso Actual' }
  ];
  public lineChartTallaNina: ChartDataSets [] = [
    { data: [.78, .86, .94, 1.01, 1.08, 1.14, 1.20, 1.24, 1.30, 1.38, 1.45, 1.48, 1.50, 1.56, 1.59, 1.61, 1.6123, 1.621, 1.625, 1.63], label: 'Estatura Mínima'},
    { data: [.92, 1.01, 1.11, 1.20,  1.28, 1.36, 1.43, 1.50, 1.57, 1.65, 1.71, 1.73, 1.74, 1.7526, 1.7536, 1.78, 1.80, 1.81, 1.817], label: 'Estatura Máxima'},
    { data: [], label: 'Estatura Actual' }
  ];
  public lineChartIMCNina: ChartDataSets [] = [
    { data: [14.07, 13.77, 13.51, 13.33, 13.22, 13.22, 13.27, 13.44, 13.76, 14.11, 14.53, 14.94, 15.44, 15.97, 16.37, 16.86, 17.97, 18.33, 18.35], label: 'IMC Mínimo'},
    { data: [19.51, 18.70, 18.61, 18.99, 19.67, 20.78, 21.87, 23.27, 24.50, 25.80, 27.05, 28.22, 29.31, 30.30, 31.30, 32.11, 32.99, 33.1, 33.19], label: 'IMC Máximo'},
    { data:  [ ], label: 'IMC Actual' }
  ];
  public lineChartLabelsnina: Label[] = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18'];
  public lineChartOptionsnina: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        },
        {
          id: 'y-axis-1',
          position: 'right',
          gridLines: {
            color: 'rgba(0,0,2555,0.2)',
          },
          ticks: {
            fontColor: 'black',
          }
        }
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  };
  public lineChartColorsNina: Color[] = [
    { // Rosa Bajito
      // backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: '#ea3af0',
      // pointBackgroundColor: '#ea3af0',
      // pointBorderColor: '#ea3af0',
      // pointHoverBackgroundColor: '#fff',
      // pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      // backgroundColor: 'rgba(0,0,255,1)',
      borderColor: ' #f40bfc',
      // pointBackgroundColor: ' #f40bfc',
      // pointBorderColor: ' #f40bfc',
      // pointHoverBackgroundColor: '#fff',
      // pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // red
      // backgroundColor: 'rgba(0,255,0,0.6)',
      borderColor: '#fc0b90',
      // pointBackgroundColor: 'rgb(241, 29, 171)',
      // pointBorderColor: 'green',
      // pointHoverBackgroundColor: 'green',
      // pointHoverBorderColor: 'rgba(0,255,0,0.6)'
    }
  ];
  //fin grafica niña peso


    
  public lineChartLabels: Label[] = [];  
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
            beginAtZero: true
        }
      }]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'horizontal',
          scaleID: 'x-axis-0',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: false,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
    plugins: {
      zoom: {
          pan: {
            enabled: true,
            drag:true,
            mode: 'xy',
            rangeMin: {
                x: null,
                y: 0
            },
            rangeMax: {
                x: null,
                y: null
            },
            speed: 20,
            threshold: 10,
          },
          zoom:{
            enabled: true,
            mode: 'y',
            rangeMin: {
                x: null,
                y: 0
            },
            rangeMax: {
                x: null,
                y: 6001
            },
            speed:0.1,
            sensitivity:0.9,
            threshold: 2,
          }
      }
  }
  };
  public lineChartColors: Color[] = [
    { // blue
      backgroundColor: 'rgba(125,138,171,0.6)',
      borderColor: 'rgba(39,60,116)',
      pointBackgroundColor: 'rgba(39,60,116)',
      pointBorderColor: 'rgba(39,60,116)',
      // pointHoverBackgroundColor: '#fff',
      // pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    {
      // backgroundColor: 'rgba(53, 126, 87, 0.9)',
      borderColor: 'red',
      pointBackgroundColor: 'red',
      pointBorderColor: 'red',
    },
    {
      borderColor: 'green',
      pointBackgroundColor: 'green',
      pointBorderColor: 'green',
    },
    {
      borderColor: 'yellow',
      pointBackgroundColor: 'yellow',
      pointBorderColor: 'yellow'
    }
  ];

  public lineChartLegend = true;
  public lineChartType: ChartType = 'line';
  public lineChartPlugins = [zoomPlugin];

  @ViewChild (BaseChartDirective, { static: true }) chart: BaseChartDirective;

  public randomize(): void {
    for (let i = 0; i < this.lineChartData.length; i++) {
      for (let j = 0; j < this.lineChartData[i].data.length; j++) {
        this.lineChartData[i].data[j] = this.generateNumber(i);
      }
    }
    this.chart.update();
  }

  private generateNumber(i: number) {
    return Math.floor((Math.random() * (i < 2 ? 100 : 1000)) + 1);
  }

  public pushOne() {
    this.lineChartData.forEach((x, i) => {
      const num = this.generateNumber(i);
      const data: number[] = x.data as number[];
      data.push(num);
    });
    this.lineChartLabels.push(`Label ${this.lineChartLabels.length}`);
  }

  //Fin Graficas
  slidesOptions = {
    slidesPerView:2
  }
}
