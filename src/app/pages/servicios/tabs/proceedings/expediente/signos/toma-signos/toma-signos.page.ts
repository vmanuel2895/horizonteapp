import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController, NavController } from '@ionic/angular';
import { PacienteService } from '../../../../../../../services/paciente/paciente.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-toma-signos',
  templateUrl: './toma-signos.page.html',
  styleUrls: ['./toma-signos.page.scss'],
})
export class TomaSignosPage implements OnInit {

  public idPaciente;
  public pacienteconsulta = {
    // los datos del paciente son solo representacionales
    nombrePaciente:"",
    apellidoPaterno:"",
    apellidoMaterno:"",
    genero:"",
    curp: "",
    id:"",
    fechaIngreso:'',
    horaIngreso:'',
    enfermeraAtendio:  "" ,
    diagnosticoActual: "",
    notaDeLaEnfermera: "",
    consultorio:"",
    doctorAPasar:'',
    consultas: 0,
    servicio:'',
    motivoIngreso:'',
    notaRecepcion:'',
    turno:'Matutino',
    status : 'APP'
  }

  public signos=[
    {
      peso: '',
      talla: '',
      pa: '',
      fc: '',
      temp: '',
      SaO: '',
      sistolica:"",
      diastolica:"",
      imc:'',
      glucosa:'',
      tomadesignos: ''
    }
  ];

  /* public paciente  = {
    nombre: '',
    apellidoPaterno: '',
    apellidoMaterno: '',
    estadoPaciente: '',
    fechaNacimiento: '',
    telefono: '',
    edad: 0,
    genero: '',
    curp:'',
    callePaciente:'',
    cpPaciente:'',
    paisPaciente:'',
    idMedicinaPreventiva: '',
    idAntecedentesHeredoFam: '',
    idPaciente:'',
    consultas: ''
  }; */

  public signosVitales = {
    talla:  0,
    peso:  0 ,
    imc: 0,
    temp: "",
    sistolica:"",
    diastolica:"",
    pao:"",
    glucosa:"",
    tomadesignos: 'APP'
  }
  public imc;
  public hora = new Date();
  public idConsulta;


  constructor(private _paciente:PacienteService, 
              private _alertCtrl: AlertController, 
              private _route:Router, 
              private _nav:NavController, 
              public modalCtrl: ModalController) { }

  ngOnInit() {
    this.obtenerIdPaciente();
    this.obtenerPaciente();
  }

  consulta(paciente){
    this._paciente.agregarConsulta(paciente)
    .subscribe((data) => {
        if( data['ok'] ){          
          this.setidConsulta(data['data']._id);
        }
      });
  }

  obtenerIMC(){    
    this.imc = (this.signosVitales.peso/(this.signosVitales.talla * this.signosVitales.talla));
    console.log(this.imc);
    
    let imcL = this.imc.toString();
  
    imcL.split(',', 2);
  
    let imcLn;
    imcLn = parseFloat(imcL).toFixed(2);
  
    console.log(imcLn);
    
    this.signosVitales.imc = imcLn;
   
  }

  setidConsulta(id){
    this.idConsulta = id;
    /* this.obtenerConsultaPorId(this.idConsulta); */
  }

  agregarSignos(){
    this._paciente.agregarSignosVitales( this.idConsulta,  this.signosVitales ).subscribe(  (data:any) => {
          if(  data['ok']){
              console.log( data );
              this.alert("", "Signos vitales guardados");
          }
    });
  }

  async alert(header, message){
    const alert = await this._alertCtrl.create({
      header: header,
      message: message,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'rojo'
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.obtenerSignosVitales();
            /* this._route.navigate(['serviciosdash/proceedings/expediente/'+this.idPaciente+'/signos']) */
          }
        }
      ]
    });
    await alert.present();
  }

  obtenerIdPaciente(){
    this.idPaciente = localStorage.getItem('idPaciente')
  }
  
  obtenerPaciente(){
    this._paciente.getPacienteBtID(  this.idPaciente )
    .subscribe( (data:any) => {      
      this.setPacienteconsulta(data.paciente);
    });
  }

  setPacienteconsulta(data){
    let arreglo = this.hora.toLocaleString('es-MX', { day: '2-digit', month: '2-digit', year: 'numeric'  }).split(' ');
    const event = new Date();
    let arreglo2 = event.toLocaleString('es-MX', { hour: 'numeric', minute: 'numeric', hour12: true }).split(' ');
    this.pacienteconsulta.nombrePaciente =data.nombrePaciente
    this.pacienteconsulta.apellidoPaterno = data.apellidoMaterno
    this.pacienteconsulta.apellidoMaterno = data.apellidoPaterno
    this.pacienteconsulta.genero= data.genero
    this.pacienteconsulta.curp = data.curp
    this.pacienteconsulta.id = data._id
    this.pacienteconsulta.consultas = data.consultas
    this.pacienteconsulta.fechaIngreso = arreglo[0] 
    this.pacienteconsulta.horaIngreso = arreglo2[0];
    this.consulta(this.pacienteconsulta);
  }

  obtenerSignosVitales(){
    this._paciente.obtenerHistroialSignosVitalesPaciente(this.idPaciente).subscribe((resp:any)=>{
      if(resp.ok){
        this.setSignos(resp['data'])
      }
    })
  }

  setSignos(resp){
    this.signos= resp;
    this.signos.reverse();
    console.log(this.signos);
    this.salirModal(this.signos);
  }

  salirModal(signos){
    this.modalCtrl.dismiss({
      'datos': signos
    });
  }

}
