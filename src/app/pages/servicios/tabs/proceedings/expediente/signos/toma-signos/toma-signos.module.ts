import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TomaSignosPageRoutingModule } from './toma-signos-routing.module';

import { TomaSignosPage } from './toma-signos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TomaSignosPageRoutingModule
  ],
  declarations: [TomaSignosPage]
})
export class TomaSignosPageModule {}
