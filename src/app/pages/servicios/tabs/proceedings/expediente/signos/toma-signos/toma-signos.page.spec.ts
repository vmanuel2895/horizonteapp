import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TomaSignosPage } from './toma-signos.page';

describe('TomaSignosPage', () => {
  let component: TomaSignosPage;
  let fixture: ComponentFixture<TomaSignosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TomaSignosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TomaSignosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
