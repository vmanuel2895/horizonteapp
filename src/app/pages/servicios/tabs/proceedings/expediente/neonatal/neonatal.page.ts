import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { PacienteService } from 'src/app/services/paciente/paciente.service';

@Component({
  selector: 'app-neonatal',
  templateUrl: './neonatal.page.html',
  styleUrls: ['./neonatal.page.scss'],
})
export class NeonatalPage implements OnInit {

  public egoCount = 0;
  public ego2Count = 0;
  public ego3Count = 0;
  public obsCount = 0;
  public ultCount=0;
  public labCount=0;
  public GOCount=0;
  public GSCount=0;
  public contador=0;
  public contadorcitas = 0;
  public paquete = {
    icon:''
  }
  public id;

  paquetes:any[] = []
  citas:any[] = []
  examenes:any[] = []

  constructor(public modalCtrl: ModalController,
              public navParams: NavParams,
              public _paciente: PacienteService) { 
    this.paquete = navParams.get('item');
    console.log(this.paquete);
    this.id = navParams.get('id');
    console.log(this.id);
        
  }

  ngOnInit() {
    this.obtenerPaquete();
  }

  obtenerPaquete(){
    this._paciente.getPaquetePaciente(this.id).subscribe((resp:any)=>{
      let encontrar = resp.data.find(element => element.paquete.nombrePaquete == "PAQUETE NEONATAL (DE 0 A 12 MESES)")
      this.setPaquete(encontrar._id);
    })
  }

  setPaquete(resp){
    this._paciente.obtenerPaquete(resp).subscribe((resp:any)=>{
      this.setPaquetePaciente(resp);
    })
    
  }



  setPaquetePaciente(resp){
      this.paquetes = resp['paquetes']
      this.citas = this.paquetes[0].visitas
      this.examenes = this.paquetes[0].examenesLab;
      this.verCosnultashechas();
  }



  verCosnultashechas(){
    this.egoCount = 0;
    this.ego2Count = 0;
    this.ego3Count = 0;
    this.citas.forEach(element => {
      // console.log( element );
      if( element.consulta == "12 Consultas con Especialista en Pediatría" ){
        // console.log("Se lo lee");
        this.egoCount += 1;
      } 
      if( element.consulta === 'Cita abierta a Urgencias con Medico General las 24 Horas'){
        this.ego2Count += 1;
      }
      if( element.consulta === '2 Estancias cortas de hasta 8 Horas en urgencias'){
        this.ego3Count += 1;
      }
    }); 
    this.verEstudios();
  }

  verEstudios(){
    this.GOCount=0;
    this.GSCount=0;
    this.examenes.forEach( (examen) => {
      if( examen.consulta === "1 Tamiz ampliado (Dentro de los primeros 10 días de nacido)" ){
        this.GOCount += 1
      }
      if( examen.consulta === "1 Grupo Sanguineo y Factor RH"  ){
        this.GSCount +=1
      }
    }); 
  }

  salirModal(){
    this.modalCtrl.dismiss();
  }

  slidesOptions = {
    slidesPerView:2
  }

}
