import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NeonatalPage } from './neonatal.page';

describe('NeonatalPage', () => {
  let component: NeonatalPage;
  let fixture: ComponentFixture<NeonatalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NeonatalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NeonatalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
