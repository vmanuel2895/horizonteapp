import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NeonatalPageRoutingModule } from './neonatal-routing.module';

import { NeonatalPage } from './neonatal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NeonatalPageRoutingModule
  ],
  declarations: [NeonatalPage]
})
export class NeonatalPageModule {}
