import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NeonatalPage } from './neonatal.page';

const routes: Routes = [
  {
    path: '',
    component: NeonatalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NeonatalPageRoutingModule {}
