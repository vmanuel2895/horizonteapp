import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, NavParams } from '@ionic/angular';
import { PagoServiciosService } from 'src/app/services/pagos/pago-servicios.service';
import { PaquetesService } from 'src/app/services/paquetes/paquetes.service';

@Component({
  selector: 'app-agregar-paquete',
  templateUrl: './agregar-paquete.page.html',
  styleUrls: ['./agregar-paquete.page.scss'],
})
export class AgregarPaquetePage implements OnInit {
  public paquetesDB= [{
    CitasIncluidas: [],
    costoTotal:0,
    examenesLaboratorio: [],
    icon: "",
    nombrePaquete: "",
    _id: ""
  }];

  public anticipo = 0;
  public costo=0;
  membresiavali = 0;
  public paqueteSelected = {
    nombrePaquete: "",
    CitasIncluidas: [],
    costoTotal:0,
    examenesLaboratorio: [],
    icon: "",
    _id: ""
  };
  public paqueteSelect;

  public infoVenta2 = {
    paciente: "",
    atendio:"",
    fecha: "",
    hora:"",
    paquete:"",
    anticipo:0,
    totalCompra:0,
    estudios:[],
    sede:"",
  }

  public infoVenta = {
    idPaciente: "",
    atendio:"",
    fecha: "",
    hora:"",
    paquete:"",
    anticipo:0,
  }

  public idPaciente;
  public hora = new Date();

  constructor(private paquetesService:PaquetesService,
              public navParams: NavParams,
              private _pagoServicios:PagoServiciosService,
              private modalCtrl: ModalController) {
    this.idPaciente = navParams.get('id');             
  }

  ngOnInit() {
    this.getPaquetes();
  }

  getPaquetes(){
    this.paquetesService.getPaquetesSolicitud().subscribe((data:any) => {
      this.paquetesDB = data.paquetes
    });
  }

  getPaquete(id:string){
    this.paquetesService.getPaqueById(id).subscribe((data)=>{
        this.paqueteSelected = data['paquete'];
          console.log(this.paqueteSelected);
        // el anticipo varia de acuerdo al paquete
        if(this.paqueteSelected.nombrePaquete === 'PAQUETE DE CONTROL PRENATAL') {
          this.membresiavali = 0;
          this.anticipo = 2500;
          /* this.pdfFilePath = "http://tlayacapan.grupomedicohorizonte.com.mx/contratos/ContratoPaqueteMaternidad.pdf"; */

        }else if( this.paqueteSelected.nombrePaquete === 'PAQUETE MÉDICO LABORAL') {
          this.membresiavali = 0;
          this.anticipo = 175;
          /* this.pdfFilePath = "http://tlayacapan.grupomedicohorizonte.com.mx/contratos/ContratoPaqueteMedicoLaboral.pdf"; */

        }else if(this.paqueteSelected.nombrePaquete === 'SERVICIOS DE LA MEMBRESÍA'){
          this.membresiavali = 1;
          this.anticipo = 580;
          /* this.pdfFilePath = "http://tlayacapan.grupomedicohorizonte.com.mx/contratos/ANEXO_PAQUETE_DE_MATERNIDAD.pdf"; */

        } else if( this.paqueteSelected.nombrePaquete === 'PAQUETE NEONATAL (DE 0 A 12 MESES)' ){
          this.membresiavali = 0;
          this.anticipo = 1000;
          /* this.pdfFilePath = "http://tlayacapan.grupomedicohorizonte.com.mx/contratos/ContratoPaqueteNeonatal.pdf"; */

        }else if( this.paqueteSelected.nombrePaquete ===  "PAQUETE VIDA PLENA"  ){
          this.membresiavali = 0;
          this.anticipo= 2800;
          /* this.pdfFilePath = "http://tlayacapan.grupomedicohorizonte.com.mx/contratos/ContratoPaqueteVidaPlena.pdf"; */

        }else if(this.paqueteSelected.nombrePaquete === 'PAQUETE DE CONTROL PRENATAL DE ALTO RIESGO'){
          this.membresiavali = 0;
          this.anticipo = 1500;
          /* this.pdfFilePath = "http://tlayacapan.grupomedicohorizonte.com.mx/contratos/ANEXO_PAQUETE_DE_MATERNIDAD.pdf"; */

        }else if(this.paqueteSelected.nombrePaquete === 'PAQUETE PEDIÁTRICO (APARTIR DE LOS 12 MESES)'){
          this.membresiavali = 0;
          this.anticipo = 1500;
          /* this.pdfFilePath = "http://tlayacapan.grupomedicohorizonte.com.mx/contratos/CONTRATODEPAQUETEPEDIATRICO.pdf"; */
        }
        console.log(this.membresiavali);  
    });
  }

  setInfoVenta(){
    this.infoVenta.atendio = 'APP';
    /* this.infoVenta.paciente = this.id; */
    this.infoVenta.idPaciente = this.idPaciente;
    this.infoVenta.paquete = this.paqueteSelected._id;
    this.infoVenta.hora = this.hora.toTimeString().split(' GM')[0];
    this.infoVenta.anticipo = this.anticipo;
    console.log( this.infoVenta );
  }

  setInfoVenta2(){
    this.infoVenta2.atendio = 'APP';
    /* this.infoVenta.paciente = this.id; */
    this.infoVenta2.paciente = this.idPaciente;
    this.infoVenta2.paquete = this.paqueteSelected._id;
    this.infoVenta2.hora = this.hora.toTimeString().split(' GM')[0];
    this.infoVenta2.anticipo = this.anticipo;
    console.log( this.infoVenta );
  }

  actualizarEstadoMembresia(){
    const body = {
      membresiaActiva: true
    }
    this.paquetesService.actualizarEstadoMembresia( this.idPaciente, body  )
                        .subscribe( data => console.log(data)  );
  }

  agregarPaqueteAlUsuario(){
    console.log( this.infoVenta  );
    this.paquetesService.agregarPaquete( this.paqueteSelected, this.idPaciente )
    .subscribe(data => {
      if(  data['ok'] ){
        console.log(data);
        this.salirModal();
        this.actualizarEstadoMembresia();
      }
    }); 
  }

  crearTablaVenta(){
    this.paquetesService.setPaquete( this.infoVenta  )
    .subscribe( data => console.log(data));
  }

  enviar(){
    this.setInfoVenta();
    this.setInfoVenta2();
      
    const carrito={
      totalSin:this.anticipo,
      totalCon:this.anticipo,
      items:[{
        nombreEstudio: this.paqueteSelected.nombrePaquete,
        precioSin:this.anticipo,
        precioCon:this.anticipo,
        name:this.paqueteSelected.nombrePaquete
      }]
    }
      
    this.infoVenta2.totalCompra=carrito.totalSin;
    this.infoVenta2.estudios= carrito.items;
    this.infoVenta2.sede = "APP"
    /* console.log(this.infoVenta); */
    this._pagoServicios.agregarPedido( this.infoVenta2 ).subscribe( (data) => {
      console.log(  data )
    });
    
    this.crearTablaVenta();
    this.agregarPaqueteAlUsuario(); 
  }

  salirModal(){
    this.modalCtrl.dismiss(location.reload());
  }
}
