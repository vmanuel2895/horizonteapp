import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AgregarPaquetePage } from './agregar-paquete.page';

const routes: Routes = [
  {
    path: '',
    component: AgregarPaquetePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AgregarPaquetePageRoutingModule {}
