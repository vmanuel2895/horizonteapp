import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AgregarPaquetePageRoutingModule } from './agregar-paquete-routing.module';

import { AgregarPaquetePage } from './agregar-paquete.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgregarPaquetePageRoutingModule
  ],
  declarations: [AgregarPaquetePage]
})
export class AgregarPaquetePageModule {}
