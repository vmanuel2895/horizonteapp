import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AgregarPaquetePage } from './agregar-paquete.page';

describe('AgregarPaquetePage', () => {
  let component: AgregarPaquetePage;
  let fixture: ComponentFixture<AgregarPaquetePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarPaquetePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AgregarPaquetePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
