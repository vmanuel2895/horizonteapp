import { Component, OnInit } from '@angular/core';
import { ServiciosService, Scv, Signos, Compras} from '../../../../../services/servicios/servicios.service';
//import { PaquetesService } from '../../../../../services/paquetes/paquetes.service';
import { ActivatedRoute, Router} from '@angular/router';
import { PacienteService } from '../../../../../services/paciente/paciente.service';
import { ModalController } from '@ionic/angular';
import { PaquetesPage } from './paquetes/paquetes.page'
import { NeonatalPage } from './neonatal/neonatal.page'
import { MedicoLaboralPage } from './medico-laboral/medico-laboral.page'
import { PediatricoPage } from './pediatrico/pediatrico.page'
import { PrenatalRiesgoPage } from './prenatal-riesgo/prenatal-riesgo.page'
import { VidaPlenaPage } from './vida-plena/vida-plena.page'
import { AgregarPaquetePage } from './agregar-paquete/agregar-paquete.page';

@Component({
  selector: 'app-expediente',
  templateUrl: './expediente.page.html',
  styleUrls: ['./expediente.page.scss'],
})
export class ExpedientePage implements OnInit {

  public id = "";
  servicio:Scv[]=[];
  signos:Signos[]=[];
  compras:Compras[]=[];
  paquetes:[];
  public paciente = {
    nombrePaciente:"",
    apellidoPaterno:"",
    apellidoMaterno: "",
    curp:"",
    telefono:0,
    consultas: 0,
    id:"",
    genero:"",
    direccion: {
      estadoPaciente:"",
      callePaciente:"",
      paisPaciente:"",
      cp:""
    },
    contactoEmergencia1Nombre:"",
    contactoEmergencia1Edad:"",
    correo: "",
    cp:"",
    edad:"",
    paquetes : [],
    membresiaHabilitada:false
  }
  constructor(private _serviciosService: ServiciosService, 
              private _paciente:PacienteService, 
              private _route: ActivatedRoute,
              private _router: Router,
              private modalCtrl:ModalController) { 
                console.log(_serviciosService.getCompras());
                
              }

  ngOnInit() {
    this.id = this._route.snapshot.paramMap.get('id');    
    this.servicio= this._serviciosService.getServicios();
    this.compras=this._serviciosService.getCompras();
    this.signos=this._serviciosService.getSignos();
    this.obtenerPaciente();
    this.setId();
  }

  obtenerPaciente(){
    this._paciente.getPacienteBtID(  this.id )
    .subscribe( (data:any) => {      
      this.paciente=data.paciente;
      console.log(this.paciente);
      
    });
  }

  setId(){
    localStorage.setItem('idPaciente', this.id)
  }

  /* getPaquetes(){
    this._paquetes.getPaquetes()
    .subscribe(  (data:any) =>  {
      console.log( data );
      this.paquetes = data['paquetes'];
      // console.log(this.paquetes);   
    });  
  } */

  slidesOptions = {
    slidesPerView:3
  }

  onClick(servicio){ 
    let id= localStorage.getItem('idPaciente')   
    if(servicio != 'ambulancia'){
      this._router.navigate(['/serv', servicio, id])
    }else{
      this._router.navigate(['/ambulancia', servicio])
    }
  } 

  async agregarPaquete(){
    let id = this.id;
      const modal = await this.modalCtrl.create({
        component: AgregarPaquetePage,
          componentProps: {
            id
          }
      });
      await modal.present();
  }
  
  async mostrarModal(item) {
    if(item.nombrePaquete == "PAQUETE NEONATAL (DE 0 A 12 MESES)"){
      let id = this.id
      const modal = await this.modalCtrl.create({
        component: NeonatalPage,
          componentProps: {
            item,
            id
          }
      });
      await modal.present();
    }else if(item.nombrePaquete == "PAQUETE DE CONTROL PRENATAL"){
      let id = this.id
      const modal = await this.modalCtrl.create({
        component: PaquetesPage,
        componentProps: {
          item,
          id
        }
      });
      await modal.present();
    }else if(item.nombrePaquete == "PAQUETE MÉDICO LABORAL"){
      let id = this.id
      const modal = await this.modalCtrl.create({
        component: MedicoLaboralPage,
        componentProps: {
          item,
          id
        }
      });
      await modal.present();
    }else if(item.nombrePaquete == "PAQUETE PEDIÁTRICO (APARTIR DE LOS 12 MESES)"){
      let id = this.id
      const modal = await this.modalCtrl.create({
        component: PediatricoPage,
        componentProps: {
          item,
          id
        }
      });
      await modal.present();
    }else if(item.nombrePaquete == "PAQUETE DE CONTROL PRENATAL DE ALTO RIESGO"){
      let id = this.id
      const modal = await this.modalCtrl.create({
        component: PrenatalRiesgoPage,
        componentProps: {
          item,
          id
        }
      });
      await modal.present();
    }else if(item.nombrePaquete == "PAQUETE VIDA PLENA"){
      let id = this.id
      const modal = await this.modalCtrl.create({
        component: VidaPlenaPage,
        componentProps: {
          item,
          id
        }
      });
      await modal.present();
    }
    
    
    
    /* const { data } = await modal.onWillDismiss();
    this.signos = data.datos
    localStorage.setItem('signos', JSON.stringify(this.signos)) */
  }

}
