import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VidaPlenaPage } from './vida-plena.page';

const routes: Routes = [
  {
    path: '',
    component: VidaPlenaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VidaPlenaPageRoutingModule {}
