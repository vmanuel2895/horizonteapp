import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VidaPlenaPageRoutingModule } from './vida-plena-routing.module';

import { VidaPlenaPage } from './vida-plena.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VidaPlenaPageRoutingModule
  ],
  declarations: [VidaPlenaPage]
})
export class VidaPlenaPageModule {}
