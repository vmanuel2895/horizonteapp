import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VidaPlenaPage } from './vida-plena.page';

describe('VidaPlenaPage', () => {
  let component: VidaPlenaPage;
  let fixture: ComponentFixture<VidaPlenaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VidaPlenaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VidaPlenaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
