import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { PacienteService } from 'src/app/services/paciente/paciente.service';
//PAQUETE VIDA PLENA
@Component({
  selector: 'app-vida-plena',
  templateUrl: './vida-plena.page.html',
  styleUrls: ['./vida-plena.page.scss'],
})
export class VidaPlenaPage implements OnInit {

  public contador = 0;
  public contadorcitas = 0;
  public egoCount = 0;
  public ego2Count = 0;
  public lab1Count =0;
  public lab2Count =0;
  public lab3Count =0;
  public lab4Count =0;
  public ray1Count =0;
  public tom1Count=0;
  public paquete = {
    icon:''
  }
  public id;

  paquetes:any[] = []
  citas:any[] = []
  examenes:any[] = []

  constructor(public modalCtrl: ModalController,
              public navParams: NavParams,
              public _paciente: PacienteService) { 
    this.paquete = navParams.get('item');
    console.log(this.paquete);
    this.id = navParams.get('id');
    console.log(this.id);
        
  }

  ngOnInit() {
    this.obtenerPaquete();
  }

  obtenerPaquete(){
    this._paciente.getPaquetePaciente(this.id).subscribe((resp:any)=>{
      let encontrar = resp.data.find(element => element.paquete.nombrePaquete == "PAQUETE VIDA PLENA")
      this.setPaquete(encontrar._id);
    })
  }

  setPaquete(resp){
    this._paciente.obtenerPaquete(resp).subscribe((resp:any)=>{
      this.setPaquetePaciente(resp);
    })
    
  }



  setPaquetePaciente(resp){
      this.paquetes = resp['paquetes']
      this.citas = this.paquetes[0].visitas
      this.examenes = this.paquetes[0].examenesLab;
      console.log(resp);
      
      this.verCosnultashechas();
  }



  verCosnultashechas(){
    this.egoCount = 0;
    this.ego2Count= 0;
    this.citas.forEach(element => {
      // console.log( element );
      if( element.consulta == "8 Consultas programadas con Especialista MEDICINA INTERNA" ){
        this.egoCount += 1;
      } 
      if( element.consulta === 'Cita abierta a urgencias desde la contratación del servicio por un año con Medicina General.'){
        this.ego2Count += 1;
      }
    });
    // termian el codigo que valida las consultas 
    this.verEstudios();
  }

  verEstudios(){
    this.lab1Count =0;
    this.lab2Count =0;
    this.lab3Count =0;
    this.lab4Count =0;
    this.ray1Count =0;
    this.tom1Count=0;
    // badgeQuimica
   //   // quimicaSanguineaCard
    this.examenes.forEach( (examen) => {
      if( examen.consulta === "1 Biometría Hemática Completa " ){
        this.lab1Count += 1;
      }
      if( examen.consulta === "1 QS 6 (Glucosa, Urea, Creatinina, Ácido Úrico, Colesterol y Triglicéridos)"  ){
        this.lab2Count += 1;
      }
      if(examen.consulta === "1 Pruebas de Funcionamiento Hepático"){
        this.lab3Count += 1;
      }
      if(examen.consulta === "1 Exámenes Generales de Orina"){
        this.lab4Count += 1;
      }
      if( examen.consulta === "1 Tele de Tórax."){
        this.ray1Count += 1;
      }
      if( examen.consulta === "1 Electrocardiograma"){
        this.tom1Count += 1
      }
    });
   //   //  termina el codigo que valida los examenes  
  }

  salirModal(){
    this.modalCtrl.dismiss();
  }

  slidesOptions = {
    slidesPerView:2
  }

}
