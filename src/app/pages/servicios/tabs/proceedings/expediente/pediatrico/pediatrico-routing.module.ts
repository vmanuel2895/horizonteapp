import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PediatricoPage } from './pediatrico.page';

const routes: Routes = [
  {
    path: '',
    component: PediatricoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PediatricoPageRoutingModule {}
