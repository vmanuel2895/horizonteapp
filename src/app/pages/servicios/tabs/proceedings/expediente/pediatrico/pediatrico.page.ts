import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { PacienteService } from 'src/app/services/paciente/paciente.service';

@Component({
  selector: 'app-pediatrico',
  templateUrl: './pediatrico.page.html',
  styleUrls: ['./pediatrico.page.scss'],
})
export class PediatricoPage implements OnInit {

  public egoCount = 0;
  public ego2Count=0;
  public ego3Count = 0;
  public ego4Count=0;
  public certificado = 0
  public contador=0;
  public contadorcitas =0;
  public abierta = 0
  public estancia = 0
  public paquete = {
    icon:''
  }
  public id;

  paquetes:any[] = []
  citas:any[] = []
  examenes:any[] = []

  constructor(public modalCtrl: ModalController,
              public navParams: NavParams,
              public _paciente: PacienteService) { 
    this.paquete = navParams.get('item');
    console.log(this.paquete);
    this.id = navParams.get('id');
    console.log(this.id);
        
  }

  ngOnInit() {
    this.obtenerPaquete();
  }

  obtenerPaquete(){
    this._paciente.getPaquetePaciente(this.id).subscribe((resp:any)=>{
      let encontrar = resp.data.find(element => element.paquete.nombrePaquete == "PAQUETE PEDIÁTRICO (APARTIR DE LOS 12 MESES)")
      this.setPaquete(encontrar._id);
    })
  }

  setPaquete(resp){
    this._paciente.obtenerPaquete(resp).subscribe((resp:any)=>{
      this.setPaquetePaciente(resp);
    })
    
  }



  setPaquetePaciente(resp){
      this.paquetes = resp['paquetes']
      this.citas = this.paquetes[0].visitas
      this.examenes = this.paquetes[0].examenesLab;
      console.log(resp);
      
      this.verCosnultashechas();
  }



  verCosnultashechas(){
    this.certificado = 0
    this.abierta = 0
    this.estancia = 0
    this.citas.forEach(element => {
      // console.log( element );
      if( element.consulta == "Cita abierta a Urgencias con Médico General" ){
        this.abierta += 1
      } 
      if( element.consulta === 'Estancias cortas de hasta 8 Horas en Urgencias'){
        this.estancia += 1
      }
      if( element.consulta === '1 Certificado Médico Escolar'){
        this.certificado += 1;
      }
    }); 
    this.verEstudios();
  }

  verEstudios(){
    this.egoCount = 0;
    this.ego2Count=0;
    this.ego3Count = 0;
    this.ego4Count=0;
   for (const iterator of this.examenes) {
      if( iterator.consulta == "1 Grupo Sanguineo y Factor RH"  ){
        this.egoCount += 1;
      }
      if( iterator.consulta == "1 Biometria Hematica Completa"  ){
        this.ego2Count += 1;
      }
      if(iterator.consulta == "2 Examen General de Orina"){
        this.ego3Count += 1;
      }
      if( iterator.consulta == "1 Examen coprológico"){
        this.ego4Count += 1;
      }
    };
  }

  salirModal(){
    this.modalCtrl.dismiss();
  }

  slidesOptions = {
    slidesPerView:2
  }

}
