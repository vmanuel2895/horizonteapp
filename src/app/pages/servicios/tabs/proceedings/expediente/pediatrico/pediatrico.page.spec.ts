import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PediatricoPage } from './pediatrico.page';

describe('PediatricoPage', () => {
  let component: PediatricoPage;
  let fixture: ComponentFixture<PediatricoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PediatricoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PediatricoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
