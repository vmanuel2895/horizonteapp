import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { PacienteService } from 'src/app/services/paciente/paciente.service';

@Component({
  selector: 'app-medico-laboral',
  templateUrl: './medico-laboral.page.html',
  styleUrls: ['./medico-laboral.page.scss'],
})
export class MedicoLaboralPage implements OnInit {

  public egoCount=0;
  public paquete = {
    icon:''
  }
  public id;

  paquetes:any[] = []
  citas:any[] = []
  examenes:any[] = []

  constructor(public modalCtrl: ModalController,
              public navParams: NavParams,
              public _paciente: PacienteService) { 
    this.paquete = navParams.get('item');
    console.log(this.paquete);
    this.id = navParams.get('id');
    console.log(this.id);
        
  }

  ngOnInit() {
    this.obtenerPaquete();
  }

  obtenerPaquete(){
    this._paciente.getPaquetePaciente(this.id).subscribe((resp:any)=>{
      let encontrar = resp.data.find(element => element.paquete.nombrePaquete == "PAQUETE MÉDICO LABORAL")
      this.setPaquete(encontrar._id);
    })
  }

  setPaquete(resp){
    this._paciente.obtenerPaquete(resp).subscribe((resp:any)=>{
      this.setPaquetePaciente(resp);
    })
    
  }



  setPaquetePaciente(resp){
      this.paquetes = resp['paquetes']
      this.citas = this.paquetes[0].visitas
      this.examenes = this.paquetes[0].examenesLab;
      console.log(resp);
      
      this.verCosnultashechas();
  }



  verCosnultashechas(){
    this.egoCount=0;
    this.citas.forEach(element => {
      if( element.consulta == "Consulta de medicina general sin costo durante un año de Lunes a Domingo las 24 Horas" ){
        this.egoCount += 1;
      } 
    });
  }

  salirModal(){
    this.modalCtrl.dismiss();
  }

  slidesOptions = {
    slidesPerView:2
  }

}
