import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MedicoLaboralPageRoutingModule } from './medico-laboral-routing.module';

import { MedicoLaboralPage } from './medico-laboral.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MedicoLaboralPageRoutingModule
  ],
  declarations: [MedicoLaboralPage]
})
export class MedicoLaboralPageModule {}
