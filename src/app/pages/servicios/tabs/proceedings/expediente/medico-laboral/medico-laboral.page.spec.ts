import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MedicoLaboralPage } from './medico-laboral.page';

describe('MedicoLaboralPage', () => {
  let component: MedicoLaboralPage;
  let fixture: ComponentFixture<MedicoLaboralPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicoLaboralPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MedicoLaboralPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
