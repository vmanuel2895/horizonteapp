import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MedicoLaboralPage } from './medico-laboral.page';

const routes: Routes = [
  {
    path: '',
    component: MedicoLaboralPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MedicoLaboralPageRoutingModule {}
