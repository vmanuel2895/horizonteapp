import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegresosServPageRoutingModule } from './regresos-serv-routing.module';

import { RegresosServPage } from './regresos-serv.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegresosServPageRoutingModule
  ],
  declarations: [RegresosServPage]
})
export class RegresosServPageModule {}
