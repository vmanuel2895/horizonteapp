import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RegresosService } from '../../../../../../services/regresos/regresos.service';

@Component({
  selector: 'app-regresos',
  templateUrl: './regresos.page.html',
  styleUrls: ['./regresos.page.scss'],
})
export class RegresosPage implements OnInit {

  public servicio = "";
  public estudios:any;
  public estudiosUltra:any;

  constructor(public _route:ActivatedRoute, public _regreso:RegresosService, public _router:Router) { }

  ngOnInit() {
    this.servicio = this._route.snapshot.paramMap.get('servicio');
    this.regresos(this.servicio); 
  }

  regresos(servicio){
    const id = localStorage.getItem('idPaciente');
    if(servicio == 'laboratorio'){
      this._regreso.getLab(id).subscribe((resp:any)=>{
        if(resp.ok){
          this.setLab(resp['data'])
        }
      })
    }
    if(servicio == 'ultrasonido'){
      this._regreso.getUltra(id).subscribe((resp:any)=>{
        if(resp.ok){
          this.setLab(resp['data'])
        }        
      })
    }
  }

  setLab(lab){
    this.estudios=lab
    console.log(this.estudios);
    
  }

  serv(servicio){
    localStorage.setItem('servicio', JSON.stringify(servicio));
    /* this._router.navigate(['regresos-serv/',this.servicio]) */
  }

}
