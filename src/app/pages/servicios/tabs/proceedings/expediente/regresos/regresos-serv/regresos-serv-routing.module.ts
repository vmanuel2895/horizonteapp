import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegresosServPage } from './regresos-serv.page';

const routes: Routes = [
  {
    path: '',
    component: RegresosServPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegresosServPageRoutingModule {}
