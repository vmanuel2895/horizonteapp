import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RegresosServPage } from './regresos-serv.page';

describe('RegresosServPage', () => {
  let component: RegresosServPage;
  let fixture: ComponentFixture<RegresosServPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegresosServPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RegresosServPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
