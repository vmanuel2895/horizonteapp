import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PDFGenerator } from '@ionic-native/pdf-generator/ngx';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import { File } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { Platform } from '@ionic/angular';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Component({
  selector: 'app-regresos-serv',
  templateUrl: './regresos-serv.page.html',
  styleUrls: ['./regresos-serv.page.scss'],
})
export class RegresosServPage implements OnInit {
  public servicio;
  public regresos:any;
  public obtenido={};
  public obtenidoF=[];
  public datos=[];
  public regreso={};
  public tipos=[];

  //make
  letterObj = {
    to: 'Hola',
    from: 'Hola',
    text: 'Hola'
  }
 
  pdfObj = null;
  constructor(public _route:ActivatedRoute, 
              private pdfGenerator: PDFGenerator, 
              private plt: Platform, 
              private file: File, 
              private fileOpener: FileOpener,
              private photoViewer: PhotoViewer) { }

  ngOnInit() {
    this.servicio = this._route.snapshot.paramMap.get('servicio');
    this.obtenerResultados();
  }
  
  obtenerResultados(){
    this.regresos= JSON.parse(localStorage.getItem('servicio'));
    console.log(this.regresos);
    if(this.servicio == 'laboratorio'){
      for (let index = 0; index < this.regresos.obtenidos.length; index++) {
        this.obtenido= this.regresos.obtenidos[index]      
      }
  
      for (const ite in this.obtenido) {
        this.obtenidoF.push(this.obtenido[ite]) 
      }
  
      for (let k = 0; k < this.regresos.idEstudio.ELEMENTOS.length; k++) {
        if (this.tipos.length == 0) {
          this.tipos.push(this.regresos.idEstudio.ELEMENTOS[k].tipo)
        } else {
          const encontrar= this.tipos.find(element => element == this.regresos.idEstudio.ELEMENTOS[k].tipo)
         if(encontrar){
            this.tipos.push("")
            // se hace un push de elementos vacios
         }else{
           // un push del elemento nuevo
           this.tipos.push(this.regresos.idEstudio.ELEMENTOS[k].tipo)
         }
        }      
      }    
      
      for (let j = 0; j < (this.regresos.idEstudio.ELEMENTOS.length); j++) {
        this.datos.push({
          "elementos" : this.regresos.idEstudio.ELEMENTOS[j].elementos,
          "tipo" : this.tipos[j],
          "referencia" : this.regresos.idEstudio.ELEMENTOS[j].referencia,
          "obtenido" : this.obtenidoF[j]
        });
      }
  
      this.regreso= this.datos
      console.log(this.regreso); 
    }
  }

  viewImage(item){
    this.photoViewer.show('https://horizonte-backend.herokuapp.com/img/'+item);
  }
  viewImage2(item){
    this.photoViewer.show(item);
  }

  /*generarPdf(){
    let element = document.getElementById('head').innerHTML;
    let elementStyle = window.getComputedStyle(element);
    console.log(elementStyle); 
        // ' <head><link rel="stylesheet" href="global.scss"></head><body> <h1> Hello World </h1></body>'
    var payload =(``+element +``  )
      let options = {
        documentSize: 'A4',
        type: 'share',
        fileName: 'Mipdf.pdf'
      }
      
      this.pdfGenerator.fromData(element, options)
      .then((stats)=> console.log('status', stats) )   // ok..., ok if it was able to handle the file to the OS.  
      .catch((err)=> console.error(err))
  }*/

  generarPdf(){
    var element  = document.getElementById('head').innerHTML
    document.body.innerHTML = element

    let options = {
      documentSize: 'A4',
      type: 'share',
      fileName: 'Mipdf.pdf'
    }

    var payload = (``+element +`` )

    this.pdfGenerator.fromData(payload,
    options)
    .then((data)=> console.log(options))
    .catch((err)=> console.log(err))
  }

  //make

  createPdf() {    
    var docDefinition = {
      content: [
        {
          columns: [
            [
              { text: 'BITCOIN', style: 'header' },
              { text: 'Cryptocurrency Payment System', style: 'sub_header' },
              { text: 'WEBSITE: https://bitcoin.org/', style: 'url' },
            ]
          ]
        },
          
        { text: 'REMINDER', style: 'header' },
        { text: new Date().toTimeString(), alignment: 'right' },
 
        { text: 'From', style: 'subheader' },
        { text: this.letterObj.from },
 
        { text: 'To', style: 'subheader' },
        this.letterObj.to,
 
        { text: this.letterObj.text, style: 'story', margin: [0, 20, 0, 20] },
 
        {
          ul: [
            'Bacon',
            'Rips',
            'BBQ',
          ]
        }
      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
        },
        subheader: {
          fontSize: 14,
          bold: true,
          margin: [0, 15, 0, 0]
        },
        story: {
          italic: true,
          alignment: 'center',
          width: '50%',
        }
      }
    }
    this.pdfObj = pdfMake.createPdf(docDefinition);
  }
 
  downloadPdf() {
    /* let estudio= this.regresos.idEstudio.ESTUDIO
    console.log(estudio.split(" ").join("")); */
    this.createPdf();
    if (this.plt.is('cordova')) {
      this.pdfObj.getBuffer((buffer) => {
        var blob = new Blob([buffer], { type: 'application/pdf' });
 
        // Save the PDF to the data Directory of our App
          this.file.writeFile(this.file.dataDirectory, 'myletter.pdf', blob, { replace: true }).then(fileEntry => {
          // Open the PDf with the correct OS tools
          this.fileOpener.open(this.file.dataDirectory + 'myletter.pdf', 'application/pdf');
        })
      });
    } else {
      // On a browser simply use download!
      this.pdfObj.download();
    }
  }

}
