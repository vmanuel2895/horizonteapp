import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegresosPage } from './regresos.page';

const routes: Routes = [
  {
    path: '',
    component: RegresosPage
  },
  {
    path: 'regresos-serv/:servicio',
    loadChildren: () => import('./regresos-serv/regresos-serv.module').then( m => m.RegresosServPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegresosPageRoutingModule {}
