import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RegresosPage } from './regresos.page';

describe('RegresosPage', () => {
  let component: RegresosPage;
  let fixture: ComponentFixture<RegresosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegresosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RegresosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
