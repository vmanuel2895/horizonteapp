import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InfoRecetasPage } from './info-recetas.page';

const routes: Routes = [
  {
    path: '',
    component: InfoRecetasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InfoRecetasPageRoutingModule {}
