import { Component, OnInit } from '@angular/core';
import { PacienteService } from '../../../../../../services/paciente/paciente.service';

@Component({
  selector: 'app-recetas',
  templateUrl: './recetas.page.html',
  styleUrls: ['./recetas.page.scss'],
})
export class RecetasPage implements OnInit {

  public idPaciente;
  public recetas;
  constructor(public _paciente:PacienteService) { }

  ngOnInit() {
    this.obtenerIdPaciente();
    this.obtenerRecetas();
  }

  obtenerIdPaciente(){
    this.idPaciente = localStorage.getItem('idPaciente');
  }

  obtenerRecetas(){
    this._paciente.getRecetas(this.idPaciente).subscribe((resp:any)=>{
      if(resp.ok){
        this.setRecetas(resp['data'])
      }
    })
  }

  setRecetas(resp){
    this.recetas = resp;
  }

  serv(item){
    localStorage.setItem('receta', JSON.stringify(item));
  }

}
