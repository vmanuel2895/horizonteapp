import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-info-recetas',
  templateUrl: './info-recetas.page.html',
  styleUrls: ['./info-recetas.page.scss'],
})
export class InfoRecetasPage implements OnInit {

  public receta;

  constructor() { }

  ngOnInit() {
    this.obtenerReceta();
  }

  obtenerReceta(){
    this.receta = JSON.parse(localStorage.getItem('receta'));
    console.log(this.receta);
    
  }

}
