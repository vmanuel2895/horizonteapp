import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InfoRecetasPageRoutingModule } from './info-recetas-routing.module';

import { InfoRecetasPage } from './info-recetas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InfoRecetasPageRoutingModule
  ],
  declarations: [InfoRecetasPage]
})
export class InfoRecetasPageModule {}
