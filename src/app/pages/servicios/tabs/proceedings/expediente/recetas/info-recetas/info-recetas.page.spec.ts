import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InfoRecetasPage } from './info-recetas.page';

describe('InfoRecetasPage', () => {
  let component: InfoRecetasPage;
  let fixture: ComponentFixture<InfoRecetasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoRecetasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InfoRecetasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
