import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PagoSemanaPage } from './pago-semana.page';

const routes: Routes = [
  {
    path: '',
    component: PagoSemanaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagoSemanaPageRoutingModule {}
