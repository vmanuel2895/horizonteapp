import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PagoSemanaPageRoutingModule } from './pago-semana-routing.module';

import { PagoSemanaPage } from './pago-semana.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PagoSemanaPageRoutingModule
  ],
  declarations: [PagoSemanaPage]
})
export class PagoSemanaPageModule {}
