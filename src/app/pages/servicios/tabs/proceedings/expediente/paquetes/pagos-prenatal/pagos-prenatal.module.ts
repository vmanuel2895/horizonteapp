import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PagosPrenatalPageRoutingModule } from './pagos-prenatal-routing.module';

import { PagosPrenatalPage } from './pagos-prenatal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PagosPrenatalPageRoutingModule
  ],
  declarations: [PagosPrenatalPage]
})
export class PagosPrenatalPageModule {}
