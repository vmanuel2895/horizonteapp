import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PagosPrenatalPage } from './pagos-prenatal.page';

const routes: Routes = [
  {
    path: '',
    component: PagosPrenatalPage
  },
  {
    path: 'pago-semana',
    loadChildren: () => import('./pago-semana/pago-semana.module').then( m => m.PagoSemanaPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagosPrenatalPageRoutingModule {}
