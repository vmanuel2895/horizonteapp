import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PagoSemanaPage } from './pago-semana.page';

describe('PagoSemanaPage', () => {
  let component: PagoSemanaPage;
  let fixture: ComponentFixture<PagoSemanaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagoSemanaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PagoSemanaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
