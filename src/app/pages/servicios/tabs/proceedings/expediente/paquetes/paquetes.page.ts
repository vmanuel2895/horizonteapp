import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { PacienteService } from 'src/app/services/paciente/paciente.service';
import { PagosPrenatalPage } from './pagos-prenatal/pagos-prenatal.page'

@Component({
  selector: 'app-paquetes',
  templateUrl: './paquetes.page.html',
  styleUrls: ['./paquetes.page.scss'],
})
export class PaquetesPage implements OnInit {

    public OBSTETRI=0;
    public ESTRUC = 0;
    public PEDI= 0;
    public adelanto=0
    public deposito =0
    public egoCount=0;
    public BHC = 0;
    public QS = 0;
    public EGO = 0;
    public VDRL = 0;
    public GS = 0;
    public VIH = 0;
    public TC = 0;
    public CTG=0;
    public EV =0;
    public URO =0;
    public contador=0;
    public contadorcitas=0;
    public GINE = 0;
    public parto=0;
    public hospi=0;
    public paquete = {
      icon:''
    }
    public id;

    public icono;
    paquetes:any[] = []
    citas:any[] = []
    examenes:any[] = []
    pagos:any[]=[]
    public idPaciente;
    public idPaquete;

  constructor(public modalCtrl: ModalController,
              public navParams: NavParams,
              public _paciente: PacienteService) { 
    this.paquete = navParams.get('item');
    this.id = navParams.get('id');    
  }

  ngOnInit() {
    this.obtenerPaquete();
  }

  obtenerPaquete(){
    this._paciente.getPaquetePaciente(this.id).subscribe((resp:any)=>{
      let encontrar = resp.data.find(element => element.paquete.nombrePaquete == "PAQUETE DE CONTROL PRENATAL")
      this.setPaquete(encontrar._id);
    })
  }

  setPaquete(resp){
    this._paciente.obtenerPaquete(resp).subscribe((resp:any)=>{
      this.setPaquetePaciente(resp);
    })
  }

  setPaquetePaciente(resp){
      this.paquetes = resp['paquetes']
      this.citas = this.paquetes[0].visitas
      this.examenes = this.paquetes[0].examenesLab;
      this.pagos = this.paquetes[0].pagos
      this.icono = this.paquetes[0].paquete.icon
      this.idPaciente = this.paquetes[0].paciente._id;
      this.idPaquete = this.paquetes[0]._id
      console.log(resp);
      this.verCosnultashechas();
  }

  async pagarPaquete(){
      let pagos = this.pagos;
      let icon = this.icono;
      let id=this.idPaquete;
      let idpaciente = this.idPaciente 
      const modal = await this.modalCtrl.create({
        component: PagosPrenatalPage,
          componentProps: {
            pagos,
            icon,
            id,
            idpaciente
          }
      });
      await modal.present();
  }

  verCosnultashechas(){
    this.GINE = 0;
    this.OBSTETRI=0;
    this.ESTRUC = 0;
    this.PEDI= 0;
    this.parto = 0;
    this.hospi=0;
    this.citas.forEach(element => {
      // console.log( element );
      if( element.consulta == "Cita abierta a urgencias desde la contratación del servicio hasta la atención del parto o cesárea por medicina general" ){
        
      } 
      if( element.consulta === 'Consulta después del parto o cesárea con medicina general' ){
        this.parto += 1;
      }
      if( element.consulta === 'Hospitalización en habitación estándar' ){
        this.hospi += 1;
      }
      if(element.consulta == '7 Consultas con especialista en Ginecología y obstetricia'){
        this.GINE += 1;
      }
      if(element.consulta == '2 Ultrasonidos obstétricos convencionales'){
        this.OBSTETRI += 1;
      }
      if(element.consulta == '1 Ultrasonido estructural'){
        this.ESTRUC += 1;
      }
      if(element.consulta == '1 consulta con pediatría antes de su parto o cesárea'){
        this.PEDI += 1;
      }
    });
    this.verEstudios();
  }

  verEstudios(){
    this.egoCount=0;
    this. BHC = 0;
    this. QS = 0;
    this. EGO = 0;
    this. VDRL = 0;
    this. GS = 0;
    this. VIH = 0;
    this. TC = 0;
    this. CTG=0;
    // badgeQuimica
   //   // quimicaSanguineaCard
   for (const iterator of this.examenes) {
     /* }
    this.examenes.forEach( (examen:consultas) => { */
      if( iterator.consulta === "2 Biometría hemática completa" ){
        this.BHC += 1;
      }
      if( iterator.consulta === "1 Química sanguínea de 6 elementos (Glucosa, Urea, Creatinina, Ácido Úrico, Colesterol y Triglicéidos )"  ){
        this.QS += 1;
      }
      if( iterator.consulta === "2 Exámen General de Orina"  ){
        this.EGO += 1;
      }
      if(iterator.consulta === "1 V.D.R.L"){
        this.VDRL += 1;
      }
      if( iterator.consulta === "1 Grupo Sanguíneo"  ){
        this.GS += 1;
      }
      if( iterator.consulta === "1 V.I.H." ){
        this.VIH += 1;
      }
      if(  iterator.consulta === "1 Tiempos de Coagulación" ){
        this.TC += 1;
      }
      if(  iterator.consulta === "1 Curva de tolerancia a la Glucosa"  ){
        this.CTG += 1;
      }
      if(  iterator.consulta === "1 Exudado Vaginal"  ){
        this.EV += 1;
      }
      if(  iterator.consulta === "1 Urocultivo"  ){
        this.URO += 1;
      }
    };
   //   //  termina el codigo que valida los examenes 
  }

  salirModal(){
    this.modalCtrl.dismiss();
  }

  slidesOptions = {
    slidesPerView:2
  }
}
