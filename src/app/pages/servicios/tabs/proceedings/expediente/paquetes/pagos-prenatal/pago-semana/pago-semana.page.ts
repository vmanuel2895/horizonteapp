import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController, NavParams } from '@ionic/angular';
import { PagoServiciosService } from 'src/app/services/pagos/pago-servicios.service';
import { PaquetesService } from 'src/app/services/paquetes/paquetes.service';

@Component({
  selector: 'app-pago-semana',
  templateUrl: './pago-semana.page.html',
  styleUrls: ['./pago-semana.page.scss'],
})
export class PagoSemanaPage implements OnInit {

  public icono;
  tabuladorPagos:any[]=[]
  public sem = [];
  pagos:any = { semanaGesta:'1', semanapago: '', pago: ''}
  public semanaGestacion=[];
  public semanapago=[];
  public acumuladoPaque = 0;
  public  paqueteAcumulado = {acumulado:0}
  public pagoFijo;
  public comparacion;
  public deposito =0
  public checked = {
    incluye: false,
    noincluye: false
  }
  public riesgo={
    valores:""
  }
  public totalpagos=0;
  public carrito = {
    totalSin: 0,
    totalCon: 0,
    items: []
  };
  public valPago=true;
  public infoVenta = {  
    paciente:"",
    nombrePaciente:"",
    vendedor:"",
    fecha:"",
    hora:"",
    estudios:[],
    efectivo:false,
    doctorQueSolicito:"",
    transferencia: false,
    tarjetCredito:false,
    tarjetaDebito:false,
    montoEfectivo:0,
    montoTarjteCredito:0,
    montoTarjetaDebito:0,
    montoTranferencia: 0,
    sede:"",
    totalCompra:0,
    prioridad:"",
    compraConMembresia:true
  }
  public hora = new Date();
  public idPaquete;
  public idPaciente;

  constructor(private modalCtrl:ModalController,
              private navParams:NavParams,
              private _paquete: PaquetesService,
              private _alertCtrl: AlertController,
              private _pagoServicios: PagoServiciosService) { 
    this.icono = navParams.get('icon');
    this.tabuladorPagos = navParams.get('tabulador');
    this.tabuladorPagos.forEach(element => {
      this.totalpagos = element.pago + this.totalpagos;
    })
    this.idPaquete = navParams.get('id');
    this.idPaciente = navParams.get('idpaciente');
  }

  ngOnInit() {
    this.semanasdegestacion(this.tabuladorPagos);
  }

  semanasdegestacion(semanas){
    const semana = semanas.reverse();
    if(semanas.length == 0){
      this.sem.push(this.pagos)
      this.generararreglo(this.sem);
    }else{
      this.sem.push(semana[0]);
      this.pagos.semanaGesta = this.sem[0].semanaGesta
      this.obtenerSemanasPago();
    }
  }
  generararreglo(sem:any){
    this.semanaGestacion=[];
    if(sem[0].semanaGesta == ""){
      for (let index = 1; index < 39; index++) {
        this.semanaGestacion.push(index);
      }
    }else{
      const semana = parseInt(sem[0].semanaGesta)+1;
      for (let index = semana; index < 39; index++) {
        this.semanaGestacion.push(index)
      }
    }
  }

  habilitarRiesgo(){
    if(this.riesgo.valores == "si"){
      this.checked.incluye = true
    }else{
      this.checked.noincluye = true
    }
  }

  limpiarradio(){
    this.riesgo.valores = ""
  }

  actualizarSemanasPago(){
    let semanapagos2 = (39 - this.sem[0].semanaGesta)
    let pagoinicial = parseFloat((15000 /semanapagos2).toFixed(2)) 
    let valor;
    let semana;
    let totalpago = 17500 - this.totalpagos
    if(this.deposito > totalpago){
      this.limpiarradio();
      this.deposito = 0;
      /* swal('Error!', 'No puede agregar depositos mayores a la cantidad restante', 'error') */
    }else{
      if(this.pagos.semanaGesta == semanapagos2){
        /* swal('Error!', 'Ya no puedes agregar deposito', 'error') */
      }else{
        if(this.checked.incluye){
          this.deposito = parseFloat((this.deposito - this.pagos.pago).toFixed(2));
          valor= this.deposito/pagoinicial
          semana = parseInt(this.pagos.semanapago) + parseInt(valor)
          this.pagos.semanapago = semana
          this.paqueteAcumulado.acumulado = this.deposito-(parseInt(valor) * parseFloat(pagoinicial.toFixed(2)))
          console.log(this.paqueteAcumulado);
          
          this.pagos.pago = parseFloat(this.pagos.pago) + this.deposito
        }else{
          valor= this.deposito/pagoinicial
          semana = parseInt(this.pagos.semanapago) + parseInt(valor)
          this.pagos.semanapago = semana
          this.paqueteAcumulado.acumulado = this.deposito - (parseInt(valor) * parseFloat(pagoinicial.toFixed(2)))
          console.log(this.paqueteAcumulado);
          this.pagos.pago = parseFloat(this.pagos.pago) + this.deposito        
        }
      }
    }
  }

  obtenerSemanasPago(){
    console.log(this.sem);
    
    if(this.sem.length == 0){
      if(this.pagos.semanaGesta == undefined){
        this.pagos.semanaGesta = '1';
      }
      this.pagos.semanapago = (39 - this.pagos.semanaGesta)
      for (let index = 1 ; index <= this.pagos.semanapago; index++) {
        this.semanapago.push(index)
      }
    }else{
      let val =0;
      if(this.sem[0].semanapago == ""){
        this.pagos.semanapago = 1
        for (let index = 1 ; index <= (39 - this.pagos.semanaGesta); index++) {
          this.semanapago.push(index)
        }
      }else{
        val = parseInt(this.sem[0].semanapago) + 1;
        this.pagos.semanapago = val
        for (let index = val ; index <= (39 - this.pagos.semanaGesta); index++) {
          this.semanapago.push(index)
        }
      }
      this.obtenerPagos(1);
    }
  }

  obtenerPagos(total){
    let semana = (39 - this.sem[0].semanaGesta)
    this.pagoFijo = this.tabuladorPagos.length
    if(this.pagos.semanapago == semana){
      this.pagos.pago = (17500- total).toFixed(2);
            
    }else{
      /* console.log('no entro'); */
      if(this.pagoFijo == 0){
        let semanapagos2 = (39 - this.sem[0].semanaGesta)
        if(this.acumuladoPaque == 0 || this.acumuladoPaque == null){
          console.log('entro1');
          
          this.pagos.pago =(this.deposito + (15000 /semanapagos2)).toFixed(2)
        }else{
          console.log('entro2');
          
          this.pagos.pago = (parseFloat((this.deposito + (15000 /semanapagos2)).toFixed(2)) - this.acumuladoPaque).toFixed(2)
          this.acumuladoPaque = 0
          this.paqueteAcumulado.acumulado = 0
        }
        this.comparacion = this.pagos.pago
      }else {
        let semanapagos = (39 - this.sem[0].semanaGesta)
        if(this.acumuladoPaque == 0 || this.acumuladoPaque == null){
          console.log('entro3');
          this.pagos.pago = (15000 /semanapagos).toFixed(2) 
        }else{
          console.log('entro4');
          this.pagos.pago = (parseFloat((15000 /semanapagos).toFixed(2)) - this.acumuladoPaque).toFixed(2) 
          console.log(parseFloat((15000 /semanapagos).toFixed(2)));
          console.log(this.acumuladoPaque.toFixed(2));
          
          this.acumuladoPaque = 0
          this.paqueteAcumulado.acumulado = 0
        }
      }
    }
    
    /* this.pagos.semanapago = ''; */
  }

  validarPago(semana, semana2, pago, pagototal){
    if(semana == semana2){
      if(pago < pagototal.toFixed(2)){
        this.valPago = false;
        return this.valPago
      }else{
        this.valPago = true;
        return this.valPago
      }
    }else{
      this.valPago = true;
      return this.valPago
    }
  }


  agregarPago(){
    if(this.pagos.tipo == '' || this.pagos.pago == '' || this.pagos.semanaGesta == '' || this.pagos.semanapago == ''){
      this.alert("Error!","Porfavor ingrese los datos que le piden");
      /* swal('Error!', 'Porfavor ingrese los datos que le piden', 'error') */
    }else{
      let semanapagos2 = (39 - this.sem[0].semanaGesta)
      if(this.pagos.semanapago > semanapagos2 ){
        this.alert("Error!","Ya no se pueden agregar mas pagos");
        /* swal('Error!', 'Ya no se pueden agregar mas pagos', 'error') */
      }else{
        let vali;
        this.pagos.pago = parseFloat(this.pagos.pago);
        let totalpago = 17500 - this.totalpagos  
        vali = this.validarPago(this.pagos.semanapago, semanapagos2, this.pagos.pago, totalpago);
        if(this.pagos.pago > totalpago || vali == false){
          this.alert("Error!","El pago no debe de ser mayor ni menor al restante");
          /* swal('Error!', 'El pago no debe de ser mayor ni menor al restante', 'error') */
          this.pagos = { semanaGesta:'', semanapago: '', pago: ''}
            this.carrito={
              totalSin: 0,
              totalCon: 0,
              items: []
            };
            this.checked = {
              incluye: false,
              noincluye: false
            }
            this.sem = [];
            this.semanapago=[];
            this.acumuladoPaque = 0
            /* this.obtenerPaquete(); */
        }else{
          this._paquete.agregarConsulta(this.pagos,'pagos',this.idPaquete).subscribe(resp =>{
            this.carrito={
              totalSin: parseFloat(this.pagos.pago),
              totalCon: parseFloat(this.pagos.pago),
              items: []
            };
            
            const item8 = {
              nombreEstudio: "Pago semana"+this.pagos.semanapago,
              precioCon:parseFloat(this.pagos.pago),
              precioSin:parseFloat(this.pagos.pago),
            }
            if(this.acumuladoPaque == 0){
              if(this.paqueteAcumulado.acumulado == 0){
                this.paqueteAcumulado.acumulado = 0
                console.log('entrooo1');
                console.log(this.paqueteAcumulado.acumulado);
                
                this._paquete.actualizarAdelanto(this.idPaquete,this.paqueteAcumulado).subscribe((resp:any)=> {
                })
              }else{
                console.log('entrooo2');
                console.log(this.paqueteAcumulado.acumulado);
                this.paqueteAcumulado.acumulado = parseFloat(this.paqueteAcumulado.acumulado.toFixed(2))
                this._paquete.actualizarAdelanto(this.idPaquete,this.paqueteAcumulado).subscribe((resp:any)=> {
                })
              } 
              
            } else{ 
              console.log('entrooo3');
                console.log(this.paqueteAcumulado.acumulado);
                this.paqueteAcumulado.acumulado = parseFloat(this.paqueteAcumulado.acumulado.toFixed(2))
              this.paqueteAcumulado.acumulado= parseFloat(this.paqueteAcumulado.acumulado.toFixed(2))             
              this._paquete.actualizarAdelanto(this.idPaquete,this.paqueteAcumulado).subscribe((resp:any)=> {
              })
            }
            this.carrito.items.push(item8);
            this.setDatesVenta();
            this._pagoServicios.agregarPedido( this.infoVenta )
              .subscribe( (resp) => {
                if(  resp['ok'] ){
                  this.alert("Pago Agregada","Se agrego el pago");
                  this.salirModal();
                  this.pagos= { semanaGesta:'', semanapago: '', pago: ''}
                }
              });
            this.pagos = { semanaGesta:'', semanapago: '', pago: ''}
            this.carrito={
              totalSin: 0,
              totalCon: 0,
              items: []
            };
            this.checked = {
              incluye: false,
              noincluye: false
            }
            this.sem = [];
            this.semanapago=[];
            this.acumuladoPaque = 0
            
            /* swal('Pago Agregada', 'se agrego el pago', 'success'); */
            
          })
          // localStorage.removeItem('folio');
        }
      }
      this.checked = {
        incluye: false,
        noincluye: false
      }
      this.sem = [];
      this.semanapago=[];
      this.acumuladoPaque = 0
      this.limpiarradio();
      this.deposito=0;
      this.sem = [];
      }
      // localStorage.setItem('folio',JSON.stringify(this.folios));
  }

  setDatesVenta(){    
    this.infoVenta.hora= this.hora.toTimeString().split(' GM')[0];
    this.infoVenta.fecha= this.hora.toISOString().split('T')[0]    
    this.infoVenta.paciente = this.idPaciente;
    this.infoVenta.sede = "APP";
    this.infoVenta.prioridad = "Programado"
    this.infoVenta.doctorQueSolicito = '';
    this.infoVenta.estudios= this.carrito.items
    this.infoVenta.totalCompra = parseFloat(this.pagos.pago);    
  }

  async alert(header, message){
    const alert = await this._alertCtrl.create({
      header: header,
      message: message,
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
            /* this.mostrarModal(); */
            
          }
          
        }
      ]
    });
    await alert.present();
  }

  salirModal(){
    this.tabuladorPagos.reverse()
    this.modalCtrl.dismiss(location.reload());
  }

}
