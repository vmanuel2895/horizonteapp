import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PaquetesPage } from './paquetes.page';

const routes: Routes = [
  {
    path: '',
    component: PaquetesPage
  },
  {
    path: 'pagos-prenatal',
    loadChildren: () => import('./pagos-prenatal/pagos-prenatal.module').then( m => m.PagosPrenatalPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PaquetesPageRoutingModule {}
