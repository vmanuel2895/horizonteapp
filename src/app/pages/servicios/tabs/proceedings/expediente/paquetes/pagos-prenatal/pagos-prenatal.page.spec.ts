import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PagosPrenatalPage } from './pagos-prenatal.page';

describe('PagosPrenatalPage', () => {
  let component: PagosPrenatalPage;
  let fixture: ComponentFixture<PagosPrenatalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagosPrenatalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PagosPrenatalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
