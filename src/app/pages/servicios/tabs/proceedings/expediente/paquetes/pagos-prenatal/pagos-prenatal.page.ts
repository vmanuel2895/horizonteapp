import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { PagoSemanaPage } from './pago-semana/pago-semana.page';

@Component({
  selector: 'app-pagos-prenatal',
  templateUrl: './pagos-prenatal.page.html',
  styleUrls: ['./pagos-prenatal.page.scss'],
})
export class PagosPrenatalPage implements OnInit {

  public icono;
  pagos:any[]=[]
  public totalPago=0+2500;
  tabuladorPagos:any[]=[]
  public idPaciente;
  public idpaciente;
  public restantes;

  constructor(public modalCtrl: ModalController,
              public navParams: NavParams) { 
    this.icono = navParams.get('icon');
    this.pagos = navParams.get('pagos');
    this.pagos.forEach(element=>{
      this.totalPago = element.pago+this.totalPago
    })
    this.tabuladorPagos = this.pagos
    this.idPaciente = navParams.get('id');
    this.idpaciente = navParams.get('idpaciente');
  }

  ngOnInit() {
    this.semanasRestantes();
  }

  semanasRestantes(){
    let cont;
    let semanapago;
    let totalsemanas;
    for (let index = 0; index < this.tabuladorPagos.length; index++) {
      cont = index
    }
    semanapago = this.tabuladorPagos[cont].semanapago;

    console.log(semanapago);
    totalsemanas = 39 - this.tabuladorPagos[0].semanaGesta;
    /* semanagesta = this.tabuladorPagos[cont].semanapago */
    this.restantes = totalsemanas - semanapago
  }

  async pagoSemana(){
    let tabulador = this.tabuladorPagos
    let icon = this.icono
    let id= this.idPaciente;
    let idpaciente = this.idpaciente
    const modal = await this.modalCtrl.create({
      component: PagoSemanaPage,
        componentProps: {
          tabulador,
          icon,
          id,
          idpaciente
        }
    });
    await modal.present();
  }

  salirModal(){
    this.modalCtrl.dismiss();
  }

}
