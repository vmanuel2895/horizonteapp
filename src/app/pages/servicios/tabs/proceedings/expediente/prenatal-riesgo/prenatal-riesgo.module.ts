import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PrenatalRiesgoPageRoutingModule } from './prenatal-riesgo-routing.module';

import { PrenatalRiesgoPage } from './prenatal-riesgo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PrenatalRiesgoPageRoutingModule
  ],
  declarations: [PrenatalRiesgoPage]
})
export class PrenatalRiesgoPageModule {}
