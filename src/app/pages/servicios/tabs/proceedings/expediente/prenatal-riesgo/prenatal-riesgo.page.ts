import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { PacienteService } from 'src/app/services/paciente/paciente.service';

@Component({
  selector: 'app-prenatal-riesgo',
  templateUrl: './prenatal-riesgo.page.html',
  styleUrls: ['./prenatal-riesgo.page.scss'],
})
export class PrenatalRiesgoPage implements OnInit {

  public contadorestudios=0;
  public contadorcitas=0;
  public egoCount = 0;
  public ego2Count = 0;
  public ult1Count = 0;
  public ult2Count = 0;
  public ult3Count = 0;
  public ult4Count = 0;
  public ult5Count = 0;
  public lab1Count = 0;
  public lab2Count = 0;
  public lab3Count = 0;
  public lab4Count = 0;
  public lab5Count = 0;
  public lab6Count = 0;
  public lab7Count = 0;
  public lab8Count = 0;
  public lab9Count = 0;
  public lab10Count = 0;
  public lab11Count = 0;
  public lab12Count = 0;
  public lab13Count = 0;
  public lab14Count = 0;
  public paquete = {
    icon:''
  }
  public id;

  paquetes:any[] = []
  citas:any[] = []
  examenes:any[] = []

  constructor(public modalCtrl: ModalController,
              public navParams: NavParams,
              public _paciente: PacienteService) { 
    this.paquete = navParams.get('item');
    console.log(this.paquete);
    this.id = navParams.get('id');
    console.log(this.id);
        
  }

  ngOnInit() {
    this.obtenerPaquete();
  }

  obtenerPaquete(){
    this._paciente.getPaquetePaciente(this.id).subscribe((resp:any)=>{
      let encontrar = resp.data.find(element => element.paquete.nombrePaquete == "PAQUETE DE CONTROL PRENATAL DE ALTO RIESGO")
      this.setPaquete(encontrar._id);
    })
  }

  setPaquete(resp){
    this._paciente.obtenerPaquete(resp).subscribe((resp:any)=>{
      this.setPaquetePaciente(resp);
    })
    
  }



  setPaquetePaciente(resp){
      this.paquetes = resp['paquetes']
      this.citas = this.paquetes[0].visitas
      this.examenes = this.paquetes[0].examenesLab;
      console.log(resp);
      
      this.verCosnultashechas();
  }



  verCosnultashechas(){
    this.egoCount= 0;
    this.ego2Count= 0;
    this.citas.forEach(element => {
      if( element.consulta == "7 Consultas programadas con el Especialista" ){
        this.egoCount += 1;
      } 
      if( element.consulta == '3 consultas de Nutrición'){
        this.ego2Count += 1;
      }
    }); 
    this.verEstudios();
  }

  verEstudios(){
    this.ult1Count = 0;
    this.ult2Count = 0;
    this.ult3Count = 0;
    this.ult4Count = 0;
    this.ult5Count = 0;
    this.lab1Count = 0;
    this.lab2Count = 0;
    this.lab3Count = 0;
    this.lab4Count = 0;
    this.lab5Count = 0;
    this.lab6Count = 0;
    this.lab7Count = 0;
    this.lab8Count = 0;
    this.lab9Count = 0;
    this.lab10Count = 0;
    this.lab11Count = 0;
    this.lab12Count = 0;
    this.lab13Count = 0;
    this.lab14Count = 0;
    // badgeQuimica
   //   // quimicaSanguineaCard
   console.log(this.examenes);
   
    this.examenes.forEach( (examen) => {
      if( examen.consulta === "1 higado y Via Biliar"  ){
        this.ult1Count += 1;
      }
      if(examen.consulta === "4 Obstétricos"){
        this.ult2Count += 1;
      }
      if(examen.consulta === "1 Genetico (11-13.6 SDG)"){
        this.ult3Count += 1;
      }
      if( examen.consulta === "1 Estructural (  18- 24 SDG)"){
        this.ult4Count += 1;
      }
      if( examen.consulta === "1 Perfil biofisico (Ultimo trimestre)"){
        this.ult5Count += 1;
      }
      //labs
      if( examen.consulta === "1 Biometría Hemática Completa "){
        this.lab1Count += 1;
      }
      if( examen.consulta === "1 QS 6 (Glucosa, Urea, Creatinina, Ácido Úrico, Colesterol y Triglicéridos)"){
        this.lab2Count += 1;
      }
      if( examen.consulta === "3 Exámenes Generales de Orina"){
        this.lab3Count += 1;
      }
      if( examen.consulta === "1 V.D.R.L."){
        this.lab4Count += 1;
      }
      if( examen.consulta === "1 V.I.H."){
        this.lab5Count += 1;
      }
      if( examen.consulta === "2 Grupos Sanguíneos (Ambos padres)"){
        this.lab6Count += 1;
      }
      if( examen.consulta === "1 Tiempos de Coagulación"){
        this.lab7Count += 1;
      }
      if( examen.consulta === "1 Curva de tolerancia a la glucosa"){
        this.lab8Count += 1;
      }
      if( examen.consulta === "1 Pruebas de funcionamiento hepático"){
        this.lab9Count += 1;
      }
      if( examen.consulta === "1 Depuración de proteínas en orina de 24 horas"){
        this.lab10Count += 1;
      }
      if( examen.consulta === "1 Triple "){
        this.lab11Count += 1;
      }
      if( examen.consulta === "1 Perfil tiroideo"){
        this.lab12Count += 1;
      }
      if( examen.consulta === "1 Urocultivo"){
        this.lab13Count += 1;
      }
      if( examen.consulta === "1 Exudado vaginal"){
        this.lab14Count += 1;
      }
    });
  }

  salirModal(){
    this.modalCtrl.dismiss();
  }

  slidesOptions = {
    slidesPerView:2
  }

}
