import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PrenatalRiesgoPage } from './prenatal-riesgo.page';

describe('PrenatalRiesgoPage', () => {
  let component: PrenatalRiesgoPage;
  let fixture: ComponentFixture<PrenatalRiesgoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrenatalRiesgoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PrenatalRiesgoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
