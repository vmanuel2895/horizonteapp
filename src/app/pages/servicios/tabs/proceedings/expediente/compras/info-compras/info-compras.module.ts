import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InfoComprasPageRoutingModule } from './info-compras-routing.module';

import { InfoComprasPage } from './info-compras.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InfoComprasPageRoutingModule
  ],
  declarations: [InfoComprasPage]
})
export class InfoComprasPageModule {}
