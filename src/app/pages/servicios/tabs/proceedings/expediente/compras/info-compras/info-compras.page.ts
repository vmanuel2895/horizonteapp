import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-info-compras',
  templateUrl: './info-compras.page.html',
  styleUrls: ['./info-compras.page.scss'],
})
export class InfoComprasPage implements OnInit {

  @Input() item: any;
  @Input() membresia: boolean;
  public total=0;
  public pago;
  constructor(public modalCtrl: ModalController, public _route:Router) { }

  ngOnInit() {
    console.log(this.item);
    console.log(this.membresia);
    for(let estudio of this.item.estudios){
      this.total = this.total + parseFloat(estudio.precioSin)
    }
    console.log(this.total);

    this.tipoPago();  
  }

  tipoPago(){
    if(this.item.efectivo == true){
      this.pago = 'Efectivo';
    }else if(this.item.tarjetCredito){
      this.pago = 'Tarjeta de Credito';
    }else if(this.item.tarjetaDebito){
      this.pago = 'Tarjeta de Debito'
    }else if(this.item.transferencia){
      this.pago = 'Transferencia'
    }else{
      this.pago = 'Pago en Sucursal'
    }
  }

  

  salirSinArgumentos() {
    this.modalCtrl.dismiss();
  }

  salirModal(){
    this.modalCtrl.dismiss();
  }

}
