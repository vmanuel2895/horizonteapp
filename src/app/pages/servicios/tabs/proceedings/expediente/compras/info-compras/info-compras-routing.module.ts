import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InfoComprasPage } from './info-compras.page';

const routes: Routes = [
  {
    path: '',
    component: InfoComprasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InfoComprasPageRoutingModule {}
