import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PacienteService } from '../../../../../../services/paciente/paciente.service';
import { ModalController } from '@ionic/angular';

import { InfoComprasPage } from './info-compras/info-compras.page';


@Component({
  selector: 'app-compras',
  templateUrl: './compras.page.html',
  styleUrls: ['./compras.page.scss'],
})
export class ComprasPage implements OnInit {

  public id = "";
  public ventas:any=[];

  constructor(private _route: ActivatedRoute,
              public _ventas:PacienteService,
              private modalCtrl:ModalController,
              public paciente: PacienteService) {}

  ngOnInit() {
    this.id = this._route.snapshot.paramMap.get('id'); 
    this._ventas.getTablaVentas(this.id).subscribe((data)=>{
      this.ventas= data 
      console.log(this.ventas);
    });
            
  }

  async mostrarModal(item,membresia:boolean) {

    const modal = await this.modalCtrl.create({
      component: InfoComprasPage,
      componentProps: {
        item,
        membresia
      }
    });
    
    await modal.present();

    // const { data } = await modal.onDidDismiss();
    const { data } = await modal.onWillDismiss();
    console.log('onWillDismiss');

    console.log(data);

  }

}
