import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InfoComprasPage } from './info-compras.page';

describe('InfoComprasPage', () => {
  let component: InfoComprasPage;
  let fixture: ComponentFixture<InfoComprasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoComprasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InfoComprasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
