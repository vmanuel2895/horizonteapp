import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExpedientePage } from './expediente.page';

const routes: Routes = [
  {
    path: '',
    component: ExpedientePage
  },
  {
    path: 'compras/:id',
    loadChildren: () => import('./compras/compras.module').then( m => m.ComprasPageModule)
  },
  {
    path: 'signos',
    loadChildren: () => import('./signos/signos.module').then( m => m.SignosPageModule)
  },
  {
    path: 'regresos/:servicio',
    loadChildren: () => import('./regresos/regresos.module').then( m => m.RegresosPageModule)
  },  {
    path: 'recetas',
    loadChildren: () => import('./recetas/recetas.module').then( m => m.RecetasPageModule)
  },
  {
    path: 'paquetes',
    loadChildren: () => import('./paquetes/paquetes.module').then( m => m.PaquetesPageModule)
  },
  {
    path: 'neonatal',
    loadChildren: () => import('./neonatal/neonatal.module').then( m => m.NeonatalPageModule)
  },
  {
    path: 'medico-laboral',
    loadChildren: () => import('./medico-laboral/medico-laboral.module').then( m => m.MedicoLaboralPageModule)
  },
  {
    path: 'pediatrico',
    loadChildren: () => import('./pediatrico/pediatrico.module').then( m => m.PediatricoPageModule)
  },
  {
    path: 'prenatal-riesgo',
    loadChildren: () => import('./prenatal-riesgo/prenatal-riesgo.module').then( m => m.PrenatalRiesgoPageModule)
  },
  {
    path: 'vida-plena',
    loadChildren: () => import('./vida-plena/vida-plena.module').then( m => m.VidaPlenaPageModule)
  },
  {
    path: 'agregar-paquete',
    loadChildren: () => import('./agregar-paquete/agregar-paquete.module').then( m => m.AgregarPaquetePageModule)
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExpedientePageRoutingModule {}
