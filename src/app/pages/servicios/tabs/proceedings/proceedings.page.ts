import { Component, OnInit } from '@angular/core';
import { ServiciosService, integrante} from '../../../../services/servicios/servicios.service';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { obtenerMembresia } from '../../../../functions/usuario';
import { PacienteService } from '../../../../services/paciente/paciente.service';
import { ChatService } from 'src/app/services/sockets/chat.service';
import { Socket } from 'ngx-socket-io';
import * as io from "socket.io-client";

@Component({
  selector: 'app-proceedings',
  templateUrl: './proceedings.page.html',
  styleUrls: ['./proceedings.page.scss'],
})
export class ProceedingsPage implements OnInit {

  nombre: string = '';
  familiar={
    usuario:[],
    integrante:"",
  };

  integrantes:any;
  public usuario = {
    RFC: '',
    curp: '',
    fechaREgistro: '',
    img: "",
    nombre: "",
    password:"" ,
    role: "",
    sede:"",
    turno:"" ,
    __v: 0,
    _id: ''
  }

  constructor(private _route:Router,  
              private paciente: PacienteService, 
              private _alertCtrl: AlertController,
              private wsloginService:ChatService,
              private socket:Socket) {
                console.log(paciente);
                
                
              }

  ngOnInit() { 
    
    if(obtenerMembresia() == "true"){
      this.alerta("Ya casi obtienes tu membresia", "Da clic en aceptar para continuar");
    }
    this.agregarFamilia();
    this.cargarFamilas();
  }  

  cargarFamilas(){
    this.paciente.cargarFamilias().subscribe((resp:any)=>{
      if (resp.ok) {
        if (resp.data.length == 0) {
          this.setFamilas([]);
        }else{
          localStorage.setItem('Familia', JSON.stringify(resp.data[0]));
          this.paciente.cargarFamilia();
          this.setFamilas(resp.data[0].integrantes);   
        }
      }
    })
  }

  setFamilas(integrantes){
    this.integrantes=integrantes    
  }

  getFamilia(){
    //this.paciente.getFamilia(localStorage)
    console.log(localStorage.getItem('Familia'));
    
  }

  agregarFamilia(){
    this.familiar.integrante="Yo";
    this.familiar.usuario.push(this.paciente.cargarPaciente())
    console.log(this.paciente.cargarPaciente());
    this.user(this.paciente.cargarPaciente());
    
  }

  user(paciente){
    this.usuario._id = paciente._id;
    this.usuario.role="user"
    this.usuario.nombre = paciente.nombrePaciente
    this.usuario.sede = "APP"
    console.log(this.usuario);
    
    this.socket.connect()
    this.wsloginService.login( this.usuario ); 
  }

  onClick(id){
    this._route.navigate(['expediente', id])
  }

  async alerta(mensaje, men){
    const alert = await this._alertCtrl.create({
      header: mensaje,
      message: men,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'rojo'
        },
        {
          text: 'Aceptar',
          handler: () => {
            this._route.navigate(['/serv', "otros"])
            localStorage.removeItem('membresia')
          }
          
        }
      ]
    });

    await alert.present();
  }

  slidesOptions = {
    slidesPerView:2
  }

}
