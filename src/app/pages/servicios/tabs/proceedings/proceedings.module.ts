import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProceedingsPageRoutingModule } from './proceedings-routing.module';

import { ProceedingsPage } from './proceedings.page';
import { ComponentsModule } from '../../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProceedingsPageRoutingModule,
    ComponentsModule
  ],
  declarations: [ProceedingsPage]
})
export class ProceedingsPageModule {}
