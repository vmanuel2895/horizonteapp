import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProceedingsPage } from './proceedings.page';

describe('ProceedingsPage', () => {
  let component: ProceedingsPage;
  let fixture: ComponentFixture<ProceedingsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProceedingsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProceedingsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
