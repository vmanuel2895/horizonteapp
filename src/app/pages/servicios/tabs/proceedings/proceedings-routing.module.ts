import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProceedingsPage } from './proceedings.page';

const routes: Routes = [
  {
    path: '',
    component: ProceedingsPage
  },
  {
    path: 'expediente/:id',
    loadChildren: () => import('./expediente/expediente.module').then( m => m.ExpedientePageModule)
  },
  {
    path: 'nuevo',
    loadChildren: () => import('./nuevo/nuevo.module').then( m => m.NuevoPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProceedingsPageRoutingModule {}
