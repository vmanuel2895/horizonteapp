import { Component, OnInit } from '@angular/core';
import { ServiciosService, Scv} from '../../../../services/servicios/servicios.service';
import { Router } from '@angular/router';
import { CarritoService } from '../../../../services/carrito/carrito.service';
import { PacienteService } from '../../../../services/paciente/paciente.service';

@Component({
  selector: 'app-servicios',
  templateUrl: './servicios.page.html',
  styleUrls: ['./servicios.page.scss'],
})
export class ServiciosPage implements OnInit {

  nombre: string = 'Servicios';
  servicio:Scv[]=[];

  constructor(public car:CarritoService ,
              private _serviciosService: ServiciosService, 
              private _route:Router,
              private paciente:PacienteService) {}

  ngOnInit() {
    this.servicio= this._serviciosService.getServicios();    
  }

  onClick(servicio,id){    
    if(servicio != 'ambulancia'){
      this._route.navigate(['/serv', servicio, id])
    }else{
      this._route.navigate(['/ambulancia', servicio])
    }
  }

}
