import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ServPage } from './serv.page';

describe('ServPage', () => {
  let component: ServPage;
  let fixture: ComponentFixture<ServPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ServPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
