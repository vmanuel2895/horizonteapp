import { Component, OnInit, ViewChild} from '@angular/core';
import { AlertController } from '@ionic/angular';
import { PopoverController } from '@ionic/angular';
import { Router, ActivatedRoute} from '@angular/router';
import { ServiciosService } from '../../../../services/servicios/servicios.service';
import { PopoverInfoComponent } from '../../../../components/popover-info/popover-info.component';
import { IonInfiniteScroll } from '@ionic/angular';
import { CarritoService } from '../../../../services/carrito/carrito.service';
import { PacienteService } from '../../../../services/paciente/paciente.service';


@Component({
  selector: 'app-serv',
  templateUrl: './serv.page.html',
  styleUrls: ['./serv.page.scss'],
})
export class ServPage implements OnInit {

  @ViewChild( IonInfiniteScroll ) infiniteScroll: IonInfiniteScroll;
  nombre: string = '';
  public servicios:string;
  serviceSi: any [] = [];
  public slice = 10
  img:string;
  public buscar = {
    estudio:''
  }

  public id: string;

  constructor(private _carrito: CarritoService, private _alertCtrl: AlertController,
               private _route:Router,public _rt:ActivatedRoute, 
               private _popoverCtrl: PopoverController, public _serv:ServiciosService,
               public paciente: PacienteService) { 
                         console.log(this.paciente);
                                  
  }

  ngOnInit() {
    this.nombre=this._rt.snapshot.paramMap.get('id');
    this.servicios = this._rt.snapshot.paramMap.get('id');
    this.id= this._rt.snapshot.paramMap.get('paciente');
    

    this.imagen(this.servicios);
    this.verServicios();
  }

  busquedaGeneral(){
    if(this.buscar.estudio.length >3){      
      this._serv.getAllDepartments(this.buscar)
      .subscribe((res:any) => {
        //console.log( res );
        if(res.data[0]!=0){
          this.serviceSi = res.data[0];
        }else{
          this.serviceSi = res.data[1];
        }            
      });
    }else if(this.buscar.estudio == ''){
      this.verServicios();
    }
  }

  verServicios(){
    console.log(this.servicios)
    if(this.servicios == 'rayosx'){
      this.servicios='xray'
    }
    this._serv.getObtener(this.servicios).subscribe(
      (res: any) => {
          
          this.serviceSi = res.data;
          console.log( res);
      },
    err => {
        console.log(<any>err);
    }
  );
  this.servicios=this._rt.snapshot.paramMap.get('id'); 
  }

  imagen(serv){
    switch(serv){
      case 'consulta':
        this.img='assets/icons/servicios/consulta/consulta.svg'
        break;
      case 'endoscopia':
        this.img='assets/icons/serviciosIconos/endoscopy.svg';
        break;
      case 'patologia':
        this.img='assets/icons/serviciosIconos/microscope.svg'
        break;
      case 'rayosx':
        this.img='assets/icons/serviciosIconos/skeleton.svg'
        break;
      case 'laboratorio':
        this.img='assets/icons/serviciosIconos/chemistry.svg'
        break;
      case 'ultrasonido':
        this.img='assets/icons/serviciosIconos/womb.svg'
        break;
      case 'tomografia':
        this.img='assets/icons/serviciosIconos/x-ray.svg'
        break;
      case 'otros':
        this.img='assets/icons/serviciosIconos/options.svg'
        break;
      default:
        break;
    }
  }

  async presentPopover(ev: any, item:any, membresia:any) {
    const popover = await this._popoverCtrl.create({
      component: PopoverInfoComponent,
      event: ev,
      mode: 'ios',
      cssClass: 'my-custom-class',
      componentProps:{
        item,
        membresia
      }
      //translucent: true,
    });

    await popover.present();
  }

  onClick(event, item: any){
    console.log(event);
    
    if(localStorage.getItem('token')){
      //this.alert("Carrito", "Se agrego correctamente al carrito");
      this._carrito.agregarCarrito(event,item);
    }else{
      this.alert("", "Es necesario registrarte para poder adquirir nuestros servicios")
    } 
  }

  async alert(header, message){
    const alert = await this._alertCtrl.create({
      header: header,
      message: message,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'rojo'
        },
        {
          text: 'Aceptar',
          handler: () => {
          }
          
        }
      ]
    });
    await alert.present();
  }

  loadData(event) {
    setTimeout(() => {
      console.log('Done');
      event.target.complete();

      this.slice = this.slice + 10
      event.target.complete();
    }, 1500);
  }

  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }

}
