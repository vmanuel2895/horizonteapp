import { Component, OnInit, ViewChild} from '@angular/core';
import { AlertController } from '@ionic/angular';
import { PopoverController } from '@ionic/angular';
import { Router, ActivatedRoute} from '@angular/router';
import { ServiciosService } from '../../../../services/servicios/servicios.service';
import { AmbulanciaComponent } from '../../../../components/popover-info/ambulancia/ambulancia.component';
import { IonInfiniteScroll } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { CarritoService } from 'src/app/services/carrito/carrito.service';

@Component({
  selector: 'app-ambulancia',
  templateUrl: './ambulancia.page.html',
  styleUrls: ['./ambulancia.page.scss'],
})
export class AmbulanciaPage implements OnInit {
  @ViewChild( IonInfiniteScroll ) infiniteScroll: IonInfiniteScroll;
  nombre: string = 'Laboratorio';
  public servicios:string;
  ambulanciaSI: any [] = [];
  public slice = 10

  constructor(private _carrito: CarritoService,private storage: Storage, private _alertCtrl: AlertController,private _route:Router,public _rt:ActivatedRoute, private _popoverCtrl: PopoverController, public _serv:ServiciosService) { }

  ngOnInit() {
    this.nombre=this._rt.snapshot.paramMap.get('id');
    this.servicios = this._rt.snapshot.paramMap.get('id');
    this.verServicios();
  }

  verServicios(){
    this._serv.getDestino(this.servicios).subscribe(
      (res: any) => {
          console.log( res );
          this.ambulanciaSI = res['data'];
      },
    err => {
        console.log(<any>err);
    });  
  }

  pedirAmbulancia(){
    window.open('https://chat.whatsapp.com/KyVNrvSJ8FCEIYOZ6d6yn4', '_system')
  }

  async presentPopover(ev: any, item:any) {
    const popover = await this._popoverCtrl.create({
      component: AmbulanciaComponent,
      event: ev,
      mode: 'ios',
      cssClass: 'my-custom-class',
      componentProps:item,
      //translucent: true,
    });

    await popover.present();
  }

  onClick(event, item: any){
    console.log(event);
    
    if(localStorage.getItem('token')){
      //this.alert("Carrito", "Se agrego correctamente al carrito");
      this._carrito.agregarCarrito(event,item);
    }else{
      this.alert("", "Es necesario registrarte para poder adquirir nuestros servicios")
    } 
  }

  async alert(header, message){
    const alert = await this._alertCtrl.create({
      header: header,
      message: message,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'rojo'
        },
        {
          text: 'Aceptar',
          handler: () => {
          }
          
        }
      ]
    });
    await alert.present();
  }

  loadData(event) {
    setTimeout(() => {
      console.log('Done');
      event.target.complete();

      this.slice = this.slice + 10
      event.target.complete();
    }, 1500);
  }

  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }
}
