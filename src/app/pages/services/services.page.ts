import { Component, OnInit, Renderer2 } from '@angular/core';
import { ServiciosService, Scv} from '../../services/servicios/servicios.service';
import { PacienteService } from '../../services/paciente/paciente.service';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-services',
  templateUrl: './services.page.html',
  styleUrls: ['./services.page.scss'],
})
export class ServicesPage implements OnInit {
  nombre: string = 'Servicios';
  servicio:Scv[]=[];

  constructor(
              private statusBar: StatusBar, 
              private _serviciosService: ServiciosService, 
              public _route:Router,
              public _paciente: PacienteService,
              public render:Renderer2
  ) 
  { 
    this.statusBar.backgroundColorByHexString('#ffffff');
  }

  ngOnInit() {
    if(this._paciente.cargarPaciente()=== null){
      this.getServicios();
    }else{
      this._route.navigate(['/serviciosdash'])
    }
  }

  getServicios(){
    this.servicio=this._serviciosService.getServicios();
  }

  onClick(servicio){
    if(servicio != 'ambulancia'){
      this._route.navigate(['/serv', servicio])
    }else{
      this._route.navigate(['/ambulancia', servicio])
    } 
  }

}
