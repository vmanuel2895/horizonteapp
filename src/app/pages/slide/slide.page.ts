import { Component, OnInit, ViewChildren } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-slide',
  templateUrl: './slide.page.html',
  styleUrls: ['./slide.page.scss'],
})
export class SlidePage implements OnInit {

  @ViewChildren('slides') slides: IonSlides;
  buttonName = "SIGUIENTE";
  selectedSlide : any;

  slid= [
    {
      img: '/assets/icons/sliders/slide1.svg',
      titulo: 'Servicios',
      desc: 'En la aplicación Horizonte puedes gestionar todos tus servicios, obtener cotizaciones desde casa, tener control de tus signos medicos básicos.',
    },
    {
      img: '/assets/icons/sliders/slide2.svg',
      titulo: 'Asistente virtual',
      desc: 'También puedes tener asistencia virtual las 24 horas del dia con los servicios y los doctores en tiempo real.',
    },
    {
      img: '/assets/icons/sliders/slide3.svg',
      titulo: 'Farmacia',
      desc: 'Contamos con un catalogo muy extenso de los productos en farmacia que obtendras en la puerta de tu casa.',
    },
    {
      img: '/assets/icons/sliders/slide4.svg',
      titulo: 'Notificaciones',
      desc: 'Te compartiremos campañas, productos nuevos y mucho más.',
    },
    {
      img: '/assets/icons/sliders/slide5.svg',
      titulo: 'Terminos y Condiciones',
      desc: 'TERMINOS Y CONDICIONES.',
    }
  ];

  constructor(private _route:Router) { }

  ionSlideChange(slides){
    this.selectedSlide= slides;
    slides.getActiveIndex().then(
      (slidesIndex)=>{
        if(slidesIndex == this.slid.length-1){
          this.buttonName = "Acepto";
        }else{
          this.buttonName = "Siguiente";
        }
      }
    );
  }

  next(){
    this.selectedSlide.getActiveIndex().then((slidesIndex)=>{
        if(slidesIndex == this.slid.length-1){
            this._route.navigate(['/nombre'])
        }else{
          this.selectedSlide.slideNext();
        }
      }
    );
  }

  ngOnInit() {
  }

}
