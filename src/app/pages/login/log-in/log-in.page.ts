import { Component, OnInit } from '@angular/core';
/* import { GooglePlus } from '@ionic-native/google-plus/ngx'; */
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.page.html',
  styleUrls: ['./log-in.page.scss'],
})
export class LogInPage implements OnInit {

  user:any={};
  constructor(private _route:Router, public _alert:AlertController, public _http:HttpClient) { }

  ngOnInit() {
    
  }

  loginGoogle(){
    /* this.googlePlus.login({}).then(res =>
     this.getData(res)

    ).catch(err => console.error(err)); */
  }

  getData(res){
    this.user=res  
    this._http.get('https://www.googleapis.com/oauth2/v3/tokeninfo?id_token='+this.user.accessToken).subscribe(
      (data:any)=>{
        this.user.name = data.displayName;
        this.user.image = data.image.url;
      });
      this._route.navigate(['/serviciosdash'])
  }
}
