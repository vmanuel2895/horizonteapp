import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { LoginServices } from '../../../services/logeo/login.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.page.html',
  styleUrls: ['./sign-in.page.scss'],
})
export class SignInPage implements OnInit {

  fechaNaci: Date = new Date();

  nombre: string = 'Registro';
  paciente = { 
    nombrePaciente:'',
    apellidoPaterno:'',
    apellidoMaterno:'',
    password: '',
    fechaNacimiento:'',
    genero:'',
    edad:'',
    correoPaciente: '',
    nomenclaturaRegistro: ''
  }
  rec_pass:string;

  constructor(private _alertCtrl: AlertController, private _route:Router, private _registro:LoginServices, private storage: Storage) { }

  ngOnInit() {
    
  }

  generarEdad(nacimiento) {
    console.log( nacimiento );
    let fecha = nacimiento;
    let splitFecha = fecha.split('-');
    var fechaActual = new Date();
    var anio = fechaActual.getFullYear();

    let edadNormal = anio - parseFloat( splitFecha[0]  );
    let edades = edadNormal.toString();
    return edades;
  }

  cambioFecha( event ) {
    this.fechaNaci= event.detail.value.split('T')[0];
  }

  async onSubmit( formulario: NgForm ) { 
    this.paciente=formulario.form.value
    this.paciente.nombrePaciente=formulario.form.value.nombrePaciente.toUpperCase();
    this.paciente.apellidoPaterno=formulario.form.value.apellidoPaterno.toUpperCase();
    this.paciente.apellidoMaterno=formulario.form.value.apellidoMaterno.toUpperCase();
    this.paciente.edad=this.generarEdad(formulario.form.value.fechaNacimiento)
    this.paciente.nomenclaturaRegistro="APP"
    //console.log(this.paciente);
    
    if(this.rec_pass != this.paciente.password){
      this.aler("Tu contraseña no coincide");
    }else{
      if(  formulario.form.value ){
        this._registro.registro(this.paciente)
        .subscribe((data) => {
          console.log(data);
          if(  data['ok'] ){
            formulario.reset();
            this.alerta("Paciente registrado");
          }
        });
      }else {
        return;
      } 
    } 
  }

  async alerta(mensaje){
    const alert = await this._alertCtrl.create({
      header: mensaje,
      message: '',
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
            this._route.navigate(['/login-correo'])
          }
          
        }
      ]
    });

    await alert.present();
  }

  async aler(mensaje){
    const alert = await this._alertCtrl.create({
      header: mensaje,
      message: '',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'rojo'
        },
        {
          text: 'Aceptar'          
        }
      ]
    });

    await alert.present();
  }

}
