import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { LoginServices } from '../../../services/logeo/login.service';


@Component({
  selector: 'app-login-correo',
  templateUrl: './login-correo.page.html',
  styleUrls: ['./login-correo.page.scss'],
})
export class LoginCorreoPage implements OnInit {

  nombre: string = '';
  usuario = { 
    correo: '',
    password: ''
  }

  constructor(private _alertCtrl: AlertController, private _route:Router, private _login:LoginServices) { }

  ngOnInit() {
  }

  async onSubmit( formulario: NgForm ) {
    //this._login.logout();

    console.log(this.usuario)
    this._login.login(this.usuario).subscribe((data:any)=>{
      console.log(data)
      //this._route.navigate(['/slide'])
      this._route.navigate(['/serviciosdash'])
    },error =>{
      if( error['ok']  == false){
        formulario.reset();
        this.alerta();
      }
    })
  }

  async alerta(){
    const alert = await this._alertCtrl.create({
      header: 'Error',
      message: 'Usuario y contraseña incorrecto',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'rojo'
        },
        {
          text: 'Aceptar',
          handler: () => {
          }
          
        }
      ]
    });

    await alert.present();
  }

}
