import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { LoginServices } from '../../../services/logeo/login.service';

@Component({
  selector: 'app-recover-pass',
  templateUrl: './recover-pass.page.html',
  styleUrls: ['./recover-pass.page.scss'],
})
export class RecoverPassPage implements OnInit {

  nombre: string = '';
  usuario = { 
    correoPaciente: '',
    password: ''
  }

  constructor(private _alertCtrl: AlertController, private _route:Router, public login:LoginServices) { }

  ngOnInit() {
  }

  async onSubmit( formulario: NgForm ) {
    console.log('entro');
    
    this.login.recuperarConstraseña(formulario.form.value).subscribe(data =>{
      console.log(data);
      
        this.alerta("Se ha cambiado tu contraseña correctamente");      
    })

    /* const alert = await this._alertCtrl.create({
      header: 'Registro correctamente',
      message: 'Te enviamos a tu correo electrónico la activación de tu cuenta',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'rojo'
        },
        {
          text: 'Aceptar',
          handler: () => {
            this._route.navigate(['/login-correo'])
          }
          
        }
      ]
    });

    await alert.present();*/
  }

  async alerta(mensaje){
    const alert = await this._alertCtrl.create({
      header: mensaje,
      message: '',
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
            this._route.navigate(['/login-correo'])
          }
          
        }
      ]
    });

    await alert.present();
  }
 
}
