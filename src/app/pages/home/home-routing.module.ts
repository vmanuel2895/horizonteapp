import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePage } from './home.page';

const routes: Routes = [
  {
    path:'home',
    component: HomePage,
    children: [
      {
        path:'servicios',
        children:[
          {
            path:'',
            loadChildren: () => import('../services/services.module').then(m => m.ServicesPageModule)
          }
        ]
      },
      /*{
        path:'login',
        children:[
          {
            path:'login',
            loadChildren: () => import('../login/log-in/log-in.module').then( m => m.LogInPageModule )
          }
        ]
      },*/
      {
        path:'buscar',
        children:[
          {
            path:'',
            loadChildren: () => import('../buscar/buscar.module').then(m => m.BuscarPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/home/servicios',
        pathMatch: 'full'
      } 
    ]
  },
  {
    path:'login',
    loadChildren: () => import('../login/log-in/log-in.module').then( m => m.LogInPageModule )
  },
  {
    path: '',
    redirectTo: '/home/servicios',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomePageRoutingModule {}
