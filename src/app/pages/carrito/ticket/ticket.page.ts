import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { CarritoService } from '../../../services/carrito/carrito.service';
import { Router } from '@angular/router';
import { numeroALetras } from '../../../functions/numero-letra';
import { PacienteService } from '../../../services/paciente/paciente.service';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.page.html',
  styleUrls: ['./ticket.page.scss'],
})
export class TicketPage implements OnInit {

  public totalLetra:string;
  constructor(public _route:Router ,
              public carrito:CarritoService, 
              public _modalCtrl:ModalController,
              private paciente: PacienteService) { 
    console.log(carrito); 
  }

  /* generatePdf(){
    var fileName = "myPdfFile.pdf";
    
    var options = {
        documentSize: 'A4',
        type: 'base64'                
    };

    var pdfhtml = '<html><body style="font-size:120%">This is the pdf content</body></html>';
            
    this.pdfGenerator.fromData(pdfhtml , options)
    .then(function(base64){               
        // To define the type of the Blob
        var contentType = "application/pdf";
            
        // if cordova.file is not available use instead :
        // var folderpath = "file:///storage/emulated/0/Download/";
        var folderpath = cordova.file.externalRootDirectory + "Download/"; //you can select other folders
        savebase64AsPDF(folderpath, fileName, base64, contentType);          
    })  
    .catch((err)=>console.log(err));
  } */

  ngOnInit() {
    if(this.paciente.usuario.membresiaActiva == false){
      this.totalLetra = numeroALetras( this.carrito.carrito.totalSin, 'Pesos mexicanos' );
    }else{
      this.totalLetra = numeroALetras( this.carrito.carrito.totalCon, 'Pesos mexicanos' );
    }
    
  }

  salirModal(){
    this._modalCtrl.dismiss();
    this.carrito.vaciarCarrito();
    this._route.navigate(['/serviciosdash/proceedings']);
  }

}
