import { Component, OnInit } from '@angular/core';
import { CarritoService } from '../../services/carrito/carrito.service';
import { AlertController, ModalController, PopoverController} from '@ionic/angular';
import { TicketPage } from './ticket/ticket.page';
import { TarjetaComponent } from '../../components/tarjeta/tarjeta.component';
import { PagoServiciosService } from '../../services/pagos/pago-servicios.service';
import { PacienteService } from '../../services/paciente/paciente.service';
import { getLocaleDateFormat } from '@angular/common';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.page.html',
  styleUrls: ['./carrito.page.scss'],
})
export class CarritoPage implements OnInit {

  public infoVenta = { 
    paciente:"",
    vendedor:"",
    fecha:"",
    hora:"",
    estudios:[],
    efectivo:false,
    doctorQueSolicito:"",
    transferencia: false,
    tarjetCredito:false,
    tarjetaDebito:false,
    montoEfectivo:0,
    montoTarjteCredito:0,
    montoTarjetaDebito:0,
    montoTranferencia: 0,
    sede:"",
    totalCompra:0
  }

  public usuario;
  public hora = new Date();
  public metodo;

  constructor(public carrito:CarritoService, 
              public _modalCtrl:ModalController, 
              public _popoverCtrl: PopoverController, 
              public _pagoServicios:PagoServiciosService,
              private _alertCtrl:AlertController,
              public paciente: PacienteService) {}

  ngOnInit() {
    
  }


  async mostrarModal() {

    const modal = await this._modalCtrl.create({
      component: TicketPage,
    });
    
    await modal.present();
  }
  
  async agregarTarjeta(value) {

    this.metodo=value;

      const popover = await this._popoverCtrl.create({
        component: TarjetaComponent,
        mode: 'ios',
        cssClass: 'my-custom-class-tarjeta',
        //componentProps:item,
        //translucent: true,
      });
  
      await popover.present();
    

  }

  valor(value){
    this.metodo=value;
    if(this.metodo == 'Tarjeta de Credito'){
      this.agregarTarjeta(this.metodo);
    }
  }

  async alert(header, message){
    const alert = await this._alertCtrl.create({
      header: header,
      message: message,
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
            this.mostrarModal();
          }
          
        }
      ]
    });
    await alert.present();
  }

  enviarPedido(){
    this.usuario = JSON.parse( localStorage.getItem('usuario') )    
    this.infoVenta.estudios = this.carrito.carrito.items;
    this.infoVenta.paciente = this.usuario._id;
    this.infoVenta.sede= "APP"
    this.infoVenta.totalCompra= this.carrito.carrito.totalSin
    this.infoVenta.hora= this.hora.toTimeString().split(' GM')[0];
    this.infoVenta.fecha= this.hora.toISOString().split('T')[0]
    //console.log(this.paciente.usuario.membresiaActiva);
    if(this.paciente.usuario.membresiaActiva == false){
      this.infoVenta.totalCompra= this.carrito.carrito.totalSin
    }else{
      this.infoVenta.totalCompra= this.carrito.carrito.totalCon 
    }
    switch(this.metodo){
      case 'Tarjeta de Credito':
        this.infoVenta.tarjetCredito= true
        break;
      case 'Tarjeta de Debito':
        this.infoVenta.tarjetaDebito= true
        break;
      case 'Efectivo':
        this.infoVenta.efectivo= true
        break;
      case 'Transferencia':
        this.infoVenta.transferencia=true
        break;
    }

    console.log(this.infoVenta);
    
    
  
    this._pagoServicios.guardarVentaSinRegistro( this.infoVenta )
      .subscribe( (data) => {
        console.log(data);
        
      if(  data['ok'] ){
        this.alert("Contacto","Te marcaremos para confirmar el pedido de tus productos");        
      }

      });
    
 }


  eliminar( id ) {
    this.carrito.eliminar(id);
  }

}

interface items {

  nombreEstudio : string
  precioCon: string
  precioSin: string
  idEstudio: string
  name: string
}