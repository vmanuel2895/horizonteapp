import { NgModule} from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
//import { MenuServicePageModule } from '../app/pages/home/tabs/menu-service/menu-service.module';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: '',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'log-in',
    loadChildren: () => import('./pages/login/log-in/log-in.module').then( m => m.LogInPageModule)
  },
  {
    path: 'sign-in',
    loadChildren: () => import('./pages/login/sign-in/sign-in.module').then( m => m.SignInPageModule)
  },
  {
    path: 'login-correo',
    loadChildren: () => import('./pages/login/login-correo/login-correo.module').then( m => m.LoginCorreoPageModule)
  },
  {
    path: 'recover-pass',
    loadChildren: () => import('./pages/login/recover-pass/recover-pass.module').then( m => m.RecoverPassPageModule)
  },
  {
    path: 'slide',
    loadChildren: () => import('./pages/slide/slide.module').then( m => m.SlidePageModule)
  },
  {
    path: 'serviciosdash',
    loadChildren: () => import('./pages/servicios/serviciosdash/serviciosdash.module').then( m => m.ServiciosdashPageModule)
  },
  {
    path: 'ambulancia/:id',
    loadChildren: () => import('./pages/servicios/servicios/ambulancia/ambulancia.module').then( m => m.AmbulanciaPageModule)
  },
  {
    path: 'servicios',
    loadChildren: () => import('./pages/servicios/tabs/servicios/servicios.module').then( m => m.ServiciosPageModule)
  },
  {
    path: 'proceedings',
    loadChildren: () => import('./pages/servicios/tabs/proceedings/proceedings.module').then( m => m.ProceedingsPageModule)
  },
  {
    path: 'ajustes',
    loadChildren: () => import('./pages/servicios/tabs/ajustes/ajustes.module').then( m => m.AjustesPageModule)
  },
  {
    path: 'carrito',
    loadChildren: () => import('./pages/carrito/carrito.module').then( m => m.CarritoPageModule)
  },
  {
    path: 'serv/:id/:paciente',
    loadChildren: () => import('./pages/servicios/servicios/serv/serv.module').then( m => m.ServPageModule)
  },   
  {
    path: 'premium',
    loadChildren: () => import('./pages/servicios/tabs/premium/premium.module').then( m => m.PremiumPageModule)
  },
  /*{
    path: 'buscar',
    loadChildren: () => import('./pages/buscar/buscar.module').then( m => m.BuscarPageModule)
  },*/
  {
    path: 'menu',
    loadChildren: () => import('./pages/menu/menu.module').then( m => m.MenuPageModule)
  },  {
    path: 'chat',
    loadChildren: () => import('./pages/servicios/tabs/chat/chat.module').then( m => m.ChatPageModule)
  }


  /*{
    path: 'services',
    loadChildren: () => import('./pages/services/services.module').then( m => m.ServicesPageModule)
  },*/

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
