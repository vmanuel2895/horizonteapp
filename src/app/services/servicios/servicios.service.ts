import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, Observer } from 'rxjs';
import { delay } from 'rxjs/operators';
import { URL } from '../../config/conf'

@Injectable({
  providedIn: 'root'
})
export class ServiciosService {
  public url= URL;
  private servicios:Scv[]=[
    {
      nombre:"Ambulancia",
      img:"/assets/icons/serviciosIconos/car.svg",
      url:"ambulancia",
      icon:"assets/icons/servicios/ambulancia/ambulance.svg"
    },
    {
      nombre:"Consulta",
      img:"/assets/icons/serviciosIconos/doctor.svg",
      url:"consulta",
      icon:"assets/icons/servicios/consulta/consulta.svg"
    },
    {
      nombre:"Endoscopia",
      img:"/assets/icons/serviciosIconos/endoscopy.svg",
      url:"endoscopia",
      icon:"assets/icons/servicios/endoscopia/endoscopia.svg"
    },
    {
      nombre:"Hospitalización",
      img:"/assets/icons/serviciosIconos/hospital-bed.svg",
      url:"hospitalizacion",
      icon:"assets/icons/servicios/hospitalizacion/hospitalizacion.svg"
    },
    {
      nombre:"Patología",
      img:"/assets/icons/serviciosIconos/microscope.svg",
      url:"patologia",
      icon:"assets/icons/servicios/patologia/patologia.svg"
    },
    {
      nombre:"Rayos-X",
      img:"/assets/icons/serviciosIconos/skeleton.svg",
      url:"rayosx",
      icon:"assets/icons/servicios/rayos-x/rayos-x.svg"
    },
    {
      nombre:"Laboratorio",
      img:"/assets/icons/serviciosIconos/chemistry.svg",
      url:"laboratorio",
      icon:"assets/icons/servicios/laboratorio/laboratorio.svg"
    },
    {
      nombre:"Quirofano",
      img:"/assets/icons/serviciosIconos/surgery-room.svg",
      url:"quirofano",
      icon:"assets/icons/servicios/quirofano/quirofano.svg"
    },
    {
      nombre:"Resonancia",
      img:"/assets/icons/serviciosIconos/mri.svg",
      url:"resonancia",
      icon:"assets/icons/servicios/resonancia/resonancia.svg"
    },
    {
      nombre:"Ultrasonido",
      img:"/assets/icons/serviciosIconos/womb.svg",
      url:"ultrasonido",
      icon:"assets/icons/servicios/ultrasonido/ultrasonido.svg"
    },
    {
      nombre:"Tomografia",
      img:"/assets/icons/serviciosIconos/x-ray.svg",
      url:"tomografia",
      icon:"assets/icons/servicios/tomografia/tomografia.svg"
    },
    {
      nombre:"Otros servicios",
      img:"/assets/icons/serviciosIconos/options.svg",
      url:"otros",
      icon:"assets/icons/servicios/otros/otros.svg"
    }
  ];

  private signos:Signos[]=[
    {
      nombre:"Signos vitales",
      url:"signos"
    },
    {
      nombre:"Historia medica",
      url:"historia"
    },
    {
      nombre:"Recetas",
      url:"recetas"
    },
    {
      nombre:"Otros",
      url:"otros"
    }
  ];

  private compras:Compras[]=[
    {
      nombre:"Tickets",
      url:"compras/"
    },
    {
      nombre:"Facturas",
      url:"tomografia"
    },
    {
      nombre:"Metodos de pago",
      url:"tomografia"
    },
    {
      nombre:"Otros",
      url:"tomografia"
    }
  ];

  private integrantes:integrante[]=[
    {
      nombre:"Julia Alvares Diaz",
      img:"/assets/persona.jpeg",
      integrante:"Madre"
    },
    {
      nombre:"Maria Montes Alvarez",
      img:"/assets/persona2.jfif",
      integrante:"Hija"
    },
    {
      nombre:"Vanessa Montes Alvarez",
      img:"/assets/persona3.jfif",
      integrante:"Hija"
    },
    {
      nombre:"Ernesto Montes Alvarez",
      img:"/assets/persona4.jpg",
      integrante:"Hijo"
    },
    {
      nombre:"Jose Montes Aragón",
      img:"/assets/persona5.jfif",
      integrante:"Padre"
    }
  ];
  

  constructor(private _http:HttpClient) {
    /* this.url = "https://horizonte-backend.herokuapp.com"; */
    /* this.url = 'http://localhost:3200'; */
  }

  getServicios(){
    return this.servicios;
  }
  getCompras(){
    return this.compras;
  }
  getSignos(){
    return this.signos;
  }
  getIntegrantes(){
    return this.integrantes;
  }
  getObtener(servicio){
    return this._http.get(this.url +'/ver/categoria/servicios/'+servicio).pipe(
      delay(1500)
    );
  }
  getDestino(slash:string): Observable<any>{
    return this._http.get(this.url +'/'+slash).pipe(
      delay(1500)
    );
  }
  getAllDepartments(buscar) {
    //console.log(buscar)
    return this._http.post(this.url+'/buscar/servicio', buscar); 
  }
}

export interface Scv{
  nombre: string;
  img: string;
  url: string;
  icon: string;
}

export interface Signos{
  nombre: string;
  url:string;
}

export interface Compras{
  nombre: string;
  url:string;
}

export interface integrante{
  nombre: string;
  img: string;
  integrante: string;
}
