import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Carrito } from '../../models/carrito/carrito.model';

@Injectable({
  providedIn: 'root'
})
export class CarritoService {

  //item: Carrito[] = [];
  carrito = {
    totalCon: 0,
    totalSin: 0,
    items: []
  };

  constructor(public _alertCtrl:AlertController) { 
    this.cargarStorage();
  }

  agregarCarrito(event, item: any ) {
    if( event.path[2].classList.contains('button-native')){
      const  estuidio = {
        nombreEstudio: item.ESTUDIO,
        precioSin: item.PRECIO_PUBLICO,
        precioCon: item.PRECIO_MEMBRESIA,
        name:  item.name,
        idEstudio: item._id
      } 
      //const nuevoItem = new Carrito(estuidio);
      this.sumarTotal(   item.PRECIO_PUBLICO, item.PRECIO_MEMBRESIA );
      this.carrito.items.push( estuidio );
      this.alert("Carrito", "El servicio se agrego correctamente al carrito") 
    }
    let carritoString = JSON.stringify( this.carrito );
    this.gaurdarCotizacion( carritoString );
  }

  eliminar( id ) {
    this.carrito.items.forEach(  (item, index) => {
      // Agregar algun otro caso que se pueda dar
      if ( index  === id ) {
        this.carrito.items.splice( id, 1 )
        if( item.precioSin && item.precioCon ){
          this.restarTotal( item.precioSin, item.precioCon );
        }else if( item.precioNoche ){
          this.restarTotal( item.precioNoche, item.precioNoche );
        }
      }
    });
      let  carritoString = JSON.stringify( this.carrito  );
      this.gaurdarCotizacion(  carritoString );
      this.alert("Carrito","El servicio se ha eliminado del carrito");
  }

  vaciarCarrito(){
    localStorage.removeItem('carrito')
    this.cargarStorage();
  }
  
  gaurdarCotizacion( carrito) {
    localStorage.setItem('carrito',  carrito);
  }

  cargarStorage() {
    if ( localStorage.getItem('carrito') ) {
      this.carrito = JSON.parse( localStorage.getItem('carrito') );
    } else {
      this.carrito={
        totalCon: 0,
        totalSin: 0,
        items: []
      }
    }
  }

  restarTotal( precioSin, precioCon  ) {
    let precioSinTrim  =  precioSin.replace('$', '');
    let precioSinComa = precioSinTrim.replace(',', '');
    // aca le quito la coma si es que trae
    let precioSinMembresiaNumber = parseFloat( precioSinComa );

    let precioConTirm = precioCon.replace('$', '');
    let precioConMembresiaSinComa = precioConTirm.replace(',', '');
      // aca le quito la coma si es que la trae
    let precioConMembresiaNumber = parseFloat( precioConMembresiaSinComa );
    
    this.carrito.totalCon = this.carrito.totalCon - precioConMembresiaNumber;
    this.carrito.totalSin = this.carrito.totalSin - precioSinMembresiaNumber;
  }

  sumarTotal(  precioSin, precioCon  ){
    // se le quitan los caracteres $ y , al precio con membresia
    let precioConMembresia  = precioCon.replace('$', '');
    let precioConSinComa  = precioConMembresia.replace(',', '');
    let precioConMembresiaNumber = parseFloat( precioConSinComa );
  
    // se le quitan los caracteres $ y , al precio sin membresia
    let costoSin = precioSin.replace('$', '');
    let costoSinComa = costoSin.replace(',', '');
    let costoSinNumber = parseFloat( costoSinComa ); 
    
    this.carrito.totalSin = this.carrito.totalSin + costoSinNumber;
    this.carrito.totalCon = this.carrito.totalCon + precioConMembresiaNumber;
  } 

  async alert(header, message){
    const alert = await this._alertCtrl.create({
      header: header,
      message: message,
      buttons: [
        {
          text: 'Aceptar',          
        }
      ]
    });
    await alert.present();
  }
}
