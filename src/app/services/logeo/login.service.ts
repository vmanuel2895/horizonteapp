import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, Observer } from 'rxjs';
import {  Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import { URL } from '../../config/conf'

@Injectable({
    providedIn: 'root'
})
  
export class LoginServices {
    public url= URL;
    public  token: string;
    public usuario: any;
    constructor(private _http:HttpClient, public _router: Router, private _storage: Storage) {
        /* this.url = "https://horizonte-backend.herokuapp.com"; */
        /* this.url = 'http://localhost:3200'; */
    }

    login(user: any) {      
        /* if( this.cargarStorage){
          this.logout();
        } */
        return this._http.post( this.url+'/log/login/app', user)
          .pipe( map( (resp: any)=> {
              localStorage.setItem('token', resp.token);
              localStorage.setItem('usuario', JSON.stringify(resp.usuario))
              this.token= localStorage.getItem('token');
              this.usuario = JSON.parse( localStorage.getItem('usuario') );
              return true;
          }) );
    
      }

    registro(data){
      let uri = `${this.url}/paciente`;
      return this._http.post( uri, data);
    }

    recuperarConstraseña(body){
      console.log(body);
      
      return this._http.put(this.url+'/actualizar/password',body)
    }

    logout(){
        this.token = null;
        this.usuario = null;
        localStorage.clear();
        /* localStorage.removeItem('token');
        localStorage.removeItem('usuario');
        localStorage.removeItem('carrito');
        localStorage.removeItem('Familia');
        localStorage.removeItem('idFamilia'); */
        this._router.navigate(['/home']);
    } 

    cargarStorage(){
      if( localStorage.getItem('token') ){

        this.token = localStorage.getItem('token');
        this.usuario = JSON.parse( localStorage.getItem('usuario') )
    
      }else {
        this.token = '';
        this.usuario = null;
        this._router.navigate(['/log-in']);
      }
    }
}