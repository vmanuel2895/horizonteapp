import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable, Observer } from 'rxjs';
import { URL } from '../../config/conf'

@Injectable({
  providedIn: 'root'
})
export class PacienteService {

  public url= URL;
  public usuario: any;
  public idFamilia:any;
  public familia:any;

  constructor(private _http:HttpClient) { 
    this.cargarPaciente();
    this.cargarIdFamilia();
    this.cargarFamilia();    
    /* this.url = 'http://localhost:3200'; */
    /* this.url = "https://horizonte-backend.herokuapp.com"; */
  }
  
  cargarPaciente(){
    if( localStorage.getItem('usuario') ){
      this.usuario = JSON.parse( localStorage.getItem('usuario') )
      return this.usuario;
    }else {
      this.usuario = null;
      return this.usuario;
    }
  }

  cargarFamilias(): Observable<any>{
    return this._http.get(this.url +'/ver/familia/'+this.usuario._id);
  }

  cargarIdFamilia(){
    if( localStorage.getItem('idFamilia') ){
      this.idFamilia = localStorage.getItem('idFamilia')      
      return this.idFamilia;
    }else {
      this.idFamilia = null;
      return this.idFamilia;
    }
  }

  cargarFamilia(){
    if(localStorage.getItem('Familia')){
      console.log('entroaqui');
      
      this.familia = JSON.parse(localStorage.getItem('Familia'));      
      return this.familia;
    }else {
      this.familia = [];
      return this.familia;
    }
  }

  getFamilia(id): Observable<any>{
    return this._http.get(this.url +'/ver/familia/uno/'+id);
  }


  getPaqueById( id ){

    let uri = `${this.url}/paquete/${id}`;
      return this._http.get( uri );
  }

  getPacienteBtID(id: string) {

    let uri = `${this.url}/pacientes/${id}`;

   return this._http.get( uri )
    .pipe( map( (paciente) =>  paciente  ) );
  }

  getTablaVentas(id:string){
    return this._http.get(this.url +'/app/ventas/usuario/'+id);
  }

  familias(body): Observable<any>{
    return this._http.post(this.url +'/nueva/familia', body);    
  }

  updateFamilias(id, param): Observable<any>{
    //let body = JSON.stringify(param);
    //console.log(body);
    return this._http.put(this.url+'/agregar/familiar/'+id, param);
  }

  getRecetas(id){
    return this._http.get(this.url +'/ver/consultas/anteriores/paciente/'+id);
  }

  obtenerHistroialSignosVitalesPaciente(id) {
    return this._http.get(this.url +'/ver/signos/paciente/'+id);
  }

  agregarConsulta( body ){
    let url = `${this.url}/consultas/general/identificacion`; 
    return this._http.post(url, body);
  }

  agregarSignosVitales(id, body){
    console.table( { id, body })
    let url =  `${this.url}/agregar/signos/consulta/${id}`;
    return this._http.put( url, body );
  }

  obtenerConsultaPorElId(id){
    let url = `${this.url}/consulta/general/identificacion/${id}`;
    return this._http.get( url );
  }

  obtenerPaquete(id){
    let url = `${this.url}/paquete/paciente/${id}`;
    return this._http.get( url );
  }

  getPaquetePaciente(paciente){
    let uri = this.url+'/ver/paquetes/paciente/'+ paciente;
    //console.log(paciente);
    return this._http.get(uri);
  }

}
