import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL } from '../../config/conf'

@Injectable({
  providedIn: 'root'
})
export class PagoServiciosService {

  /* public url = "http://localhost:3200";  */
  public url = URL;

  constructor( private _http: HttpClient  ) { }

  public agregarPedido(venta){
    
    let uri  =  `${this.url}/venta/servicios`;
    return this._http.post( uri, venta );

  }

  //  sin registro del paciente 
  public guardarVentaSinRegistro( venta ){
    let url = `${this.url}/venta/servicios`;
    return this._http.post( url, venta );
  }

  pedidosLaboratorio( pedido  ){
    let url = `${this.url}/crear/pedido`;
    return this._http.post( url, pedido  );
  }

}
