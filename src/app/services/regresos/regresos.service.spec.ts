import { TestBed } from '@angular/core/testing';

import { RegresosService } from './regresos.service';

describe('RegresosService', () => {
  let service: RegresosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RegresosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
