import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable, Observer } from 'rxjs';
import { URL } from '../../config/conf'

@Injectable({
  providedIn: 'root'
})
export class RegresosService {

  public url= URL;

  constructor(private _http:HttpClient) { 
    /* this.url = 'http://localhost:3200'; */
    /* this.url = "https://horizonte-backend.herokuapp.com"; */
  }

  getLab(id): Observable<any>{
    return this._http.get(this.url +'/ver/estudio/laboratorio/'+id);
  }
  getUltra(id): Observable<any>{
    return this._http.get(this.url +'/ver/estudio/regreso/ultrasonido/hc/'+id);
  }

}
