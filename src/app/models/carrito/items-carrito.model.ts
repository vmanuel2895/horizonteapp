export class ItemsCarrito {

    idEstudio: string;
    name: string;
    nombreEstudio: string;
    precioCon: string;
    precioSin: string;

}

export class Item{
    ESTUDIO: string;
    PRECIO_MEMBRESIA: string;
    PRECIO_MEMBRESIA_HOSPITALIZACION: string;
    PRECIO_MEMBRESIA_HOSPITALIZACION_URGENCIA: string;
    PRECIO_MEMBRESIA_URGENCIA: string;
    PRECIO_PUBLICO: string;
    PRECIO_PUBLICO_HOSPITALIZACION: string;
    PRECIO_PUBLICO_HOSPITALIZACIO_URGENCIA: string;
    PRECIO_PUBLICO_URGENCIA: string;
    name: string;
    __v: number;
    _id: string;
}

export class Estudio{
    nombreEstudio: string;
    precioSin: string;
    precioCon: string;
    name:  string;
    idEstudio: string;
}